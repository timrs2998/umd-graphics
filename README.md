## UMD Graphics Raytracer and Rasterizer ##

[![pipeline status](https://gitlab.com/timrs2998/umd-graphics/badges/master/pipeline.svg)](https://gitlab.com/timrs2998/umd-graphics/commits/master)

A software raytracer and rasterizer created for CS 5721 Graphics at the 
University of MN Duluth under Dr. Pete Willemsen. You can follow the 
instructions below to compile and run the code.

### Dependencies ###

In Ubuntu 14.04, you can install all of the required dependencies like so:

    sudo apt-get install cmake git g++ libpng-dev libz-dev libboost-all-dev libXi-dev libXmu-dev freeglut3-dev libglew-dev mesa-utils libsfml-dev

### Compiling ###

The project uses cmake and can be built like most cmake projects.

    git clone https://gitlab.com/timrs2998/umd-graphics.git
    cd umd-graphics/
    mkdir build
    cd build
    cmake ..
    make -j

### Running ###

By default, the raytracer is used to render scenes. You can use ```-m raster```
to render with the software rasterizer. See all options with ```--help```. The
scenes are stored in subfolders in the cs5721Lib/ directory.

Below is an example of running the raytracer from the build directory with 8 threads and 4 rays per pixel, producing an 800x800 image:

    # Run the raytracer (this may take a while)
    ./src/renderer -w 800 -h 800 -n 8 -r 4 -i ../cs5721Lib/SceneFiles_TestB/metallicDragons.xml
    
    # Open the image
    xdg-open output.png

### Sources ###

The Fundamentals of Computer Graphics, 3rd edition, by Peter Shirley
and Steve Marschner was used as a reference. Its ISBN-13 is 978-1568814698. The
code in the cs5721Lib/ directory was mostly written by Dr. Pete Willemsen.

### Licensing ###

All code under src/ and most code under cs5721Lib/ is licensed under the 
GNU GENERAL PUBLIC LICENSE Version 3.

### OpenGL ###

The following is not required to build the renderer binary. For the lab code, 
either use mesa or install nvidia drivers.

    # Install OpenGL drivers
    sudo add-apt-repository ppa:ubuntu-x-swat/x-updates
    sudo apt-get update
    sudo apt-get install nvidia-331-dev
    sudo apt-get dist-upgrade
    # Reboot
