#pragma once

#include "Basis.h"
#include <Vector3D.h>


namespace sivelab {
    class Ray;

    class Camera {
    public:
        Camera(Vector3D position, Vector3D viewDir, double focalLength, double imagePlaneWidth, int nx, int ny);
        Camera(Vector3D position, double focalLength, double imagePlaneWidth, int nx, int ny, Vector3D lookatPoint);
        virtual ~Camera();
        void computeRay(Ray& ray, const double x, const double y) const;

        // Getters
        const Vector3D& getPosition() const;
        const Basis& getBasis() const;
        const Vector3D& getLookAt() const;
        const double getFocalLength() const;
        double getImagePlaneHeight() const;
        const double getImagePlaneWidth() const;

    private:
        const Vector3D e;
        const double focalLength, imagePlaneWidth, nx, ny;
        Vector3D lookat;
        double imagePlaneHeight;
        Basis basis;
    };
}
