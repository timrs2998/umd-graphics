#include "Scene.h"

#include <BlinnPhongMirrored.h>
#include <Box.h>
#include <Dielectric.h>
#include <Glaze.h>
#include <NormalMap.h>
#include <Phong.h>
#include <Sphere.h>
#include <Triangle.h>
#include <InstancedObject.h>
#include "../textures/Texture.h"
#include "model_obj.h"
#include "lights/Area.h"

namespace sivelab {
    void Scene::parseSceneData(ptree::value_type const &v) {
        using std::string;

        std::istringstream buf;
        buf.str(v.second.get<string>("bgColor"));
        buf >> bgColor;
        buf.clear();
    }

    void Scene::parseTextureData(ptree::value_type const &v) {
        using std::string;

        string name, type;
        name = v.second.get<string>("<xmlattr>.name");
        type = v.second.get<string>("<xmlattr>.type");

        // Get a pointer to a shape
        Texture* texture = NULL;
        std::istringstream buf;
        if (type == "perlinNoise" || type == "PerlinNoise" || type == "Wood"
            || type == "wood") {

        } else if (type == "image") {
            string filename;

            buf.str(v.second.get<string>("sourcefile"));
            buf >> filename;
            buf.clear();

            // Try to construct the directory from the current scene's filename
            string directory = this->filename.substr(0,
                                                     this->filename.find_last_of("\\/"));
            string texture_file = directory + "/" + filename;
            std::cout << "Loading: " << texture_file << std::endl;

            png::image<png::rgb_pixel> textureImage;
            textureImage.read(texture_file);
            int imageWidth = textureImage.get_width();
            int imageHeight = textureImage.get_height();

            Vector3D** colors = new Vector3D*[imageWidth];
            for (int i = 0; i < imageWidth; i++) {
                colors[i] = new Vector3D[imageHeight];
                for (int j = 0; j < imageHeight; j++) {
                    png::rgb_pixel texel = textureImage[j][i];
                    colors[i][j] = Vector3D(texel.red, texel.green, texel.blue);
                }
            }
            texture = new Texture(colors, imageWidth, imageHeight);
        } else if (type == "Marble") {

        }

        assert(texture != NULL);
        textureMap[name] = texture;
    }

    void Scene::parseShapeData(ptree::value_type const &v) {
        using std::string;

        string name, type;
        name = v.second.get<string>("<xmlattr>.name");
        type = v.second.get<string>("<xmlattr>.type");

        // Get a pointer to the associated shader
        ptree::const_assoc_iterator it;
        Shader* shader = NULL;
        it = v.second.find("shader");
        if (it != v.second.not_found()) {
            shader = parseShaderData(*it);
        }

        // Get a pointer to the associated transformation
        Matrix4x4 transformation;
        transformation.setIdentity();
        it = v.second.find("transform");
        if (it != v.second.not_found()) {
            transformation = parseTransformData(*it);
        }

        // Get a pointer to a shape
        Shape* shape = NULL;
        std::istringstream buf;
        if (type == "sphere") {
            double radius;
            Vector3D center;

            buf.str(v.second.get<string>("center"));
            buf >> center;
            buf.clear();

            radius = v.second.get<double>("radius");

            shape = new Sphere(center, radius);
        } else if (type == "box") {
            Vector3D minPoint, maxPoint;

            buf.str(v.second.get<string>("minPt"));
            buf >> minPoint;
            buf.clear();

            buf.str(v.second.get<string>("maxPt"));
            buf >> maxPoint;
            buf.clear();

            shape = new Box(minPoint, maxPoint);
        } else if (type == "triangle") {
            Vector3D v0, v1, v2;

            buf.str(v.second.get<string>("v0"));
            buf >> v0;
            buf.clear();

            buf.str(v.second.get<string>("v1"));
            buf >> v1;
            buf.clear();

            buf.str(v.second.get<string>("v2"));
            buf >> v2;
            buf.clear();

            shape = new Triangle(v0, v1, v2);
        } else if (type == "mesh") {
            std::cout << "Loading " << name << std::endl;
            string filename = v.second.get<string>("file");
            shape = loadMesh(filename);
        } else if (type == "instance") {
            boost::optional<string> optInstanceId = v.second.get_optional<string>(
                    "<xmlattr>.id");
            Shape* renderable = instanceMap[*optInstanceId];
            assert(renderable != NULL);
            shape = new InstancedObject(renderable, transformation);

            if (shader == NULL) {
                shader = renderable->getShader();
            }
        } else {
            std::cerr << "Unknown shape." << std::endl;
        }
        assert(shape != NULL);
        assert(shader != NULL);

        shape->setShader(shader);
        if (v.first == "instance") {
            instanceMap[name] = shape;
        } else {
            shapes.push_back(shape);
        }
    }

    void Scene::parseLightData(ptree::value_type const &value) {
        using std::string;
        string type, name;
        type = value.second.get<string>("<xmlattr>.type");
        // name = value.second.get<string>("<xmlattr>.name");

        std::istringstream buf;

        if (type == "point") {
            Vector3D position, intensity;

            buf.str(value.second.get<std::string>("position"));
            buf >> position;
            buf.clear();

            buf.str(value.second.get<std::string>("intensity"));
            buf >> intensity;
            buf.clear();

            Light* light = new Light(position, intensity);
            lights.push_back(light);
        } else if (type == "area") {
            double width, length;
            Vector3D position, intensity, normal;

            buf.str(value.second.get<std::string>("position"));
            buf >> position;
            buf.clear();

            buf.str(value.second.get<std::string>("intensity"));
            buf >> intensity;
            buf.clear();

            buf.str(value.second.get<std::string>("normal"));
            buf >> normal;
            buf.clear();

            width = value.second.get<double>("width");
            length = value.second.get<double>("length");
            Area* area = new Area(position, intensity, normal, width, length, N);
            areaLights.push_back(area);
        } else {
            std::cout << "Unknown light." << std::endl;
        }
    }

    Shader* Scene::parseShaderData(ptree::value_type const &v) {
        using std::string;
        std::istringstream buf;

        string type, name;
        boost::optional<string> optShaderRef = v.second.get_optional<string>(
                "<xmlattr>.ref");

        if (!optShaderRef) {
            type = v.second.get<string>("<xmlattr>.type");
            name = v.second.get<string>("<xmlattr>.name");
        }

        // if name exists in the shader map and this is a ref, return the shader pointer
        // otherwise, add the shader if it NOT a ref and the name doesn't exist
        // final is to throw error

        if (!optShaderRef) {
            Shader* shader = NULL;

            // Did not find the shader and it was not a reference, so create and return
            if (type == "Lambertian") {
                Vector3D kd;
                buf.str(v.second.get<std::string>("diffuse"));
                buf >> kd;
                buf.clear();

                boost::optional<std::string> pTexName = v.second.get_optional<
                        std::string>("diffuse.<xmlattr>.tex");
                if (pTexName) {
                    Texture* texture = textureMap[pTexName.get()];
                    // TODO: restore this code once more shapes have textures
                    // shader = new Lambertian(texture);
                } else {
                    // shader = new Lambertian(kd);
                }
                shader = new Lambertian(kd);
            } else if (type == "LambertianPT") {
                Vector3D kd;
                buf.str(v.second.get<std::string>("diffuse"));
                buf >> kd;
                buf.clear();

                boost::optional<std::string> pTexName = v.second.get_optional<
                        std::string>("diffuse.<xmlattr>.tex");
                if (pTexName) {
                    // find the texture ptr
                    ;
                } else {
                    ;
                }
            } else if (type == "CoolToWarm") {
                Vector3D kd;
                buf.str(v.second.get<std::string>("diffuse"));
                buf >> kd;
                buf.clear();

                // std::string texName = v.second.get<std::string>("diffuse.<xmlattr>.tex");
                boost::optional<std::string> pTexName = v.second.get_optional<
                        std::string>("diffuse.<xmlattr>.tex");
                if (pTexName) {
                    // find the texture ptr
                    ;
                } else {
                    ;
                }
            } else if (type == "BlinnPhong" || type == "Phong") {
                Vector3D d, s;
                double phongExp;

                buf.str(v.second.get<std::string>("diffuse"));
                buf >> d;
                buf.clear();

                boost::optional<std::string> pTexName = v.second.get_optional<
                        std::string>("diffuse.<xmlattr>.tex");
                if (pTexName) {
                    Texture* texture = textureMap[pTexName.get()];
                    // TODO: construct BlinnPhong with diffuse texture
                } else {
                    ;
                }

                buf.str(v.second.get<std::string>("specular"));
                buf >> s;
                buf.clear();

                pTexName = v.second.get_optional<std::string>(
                        "specular.<xmlattr>.tex");
                if (pTexName) {
                    Texture* texture = textureMap[pTexName.get()];
                    // TODO: construct BlinnPhong with specular texture
                } else {
                    ;
                }

                phongExp = v.second.get<double>("phongExp");

                if (type == "BlinnPhong") {
                    shader = new BlinnPhong(d, s, phongExp);
                } else {
                    shader = new Phong(d, s, phongExp);
                }
            }
            else if (type == "Mirror") {
                shader = new Mirror();
            }
            else if (type == "Glaze") {
                Vector3D kd;
                buf.str(v.second.get<std::string>("diffuse"));
                buf >> kd;
                buf.clear();

                double mirrorCoef = 1.0;
                buf.str(v.second.get<std::string>("mirrorCoef"));
                buf >> mirrorCoef;
                buf.clear();

                shader = new Glaze(kd, mirrorCoef);
            } else if (type == "BlinnPhongMirrored") {
                Vector3D kd;
                buf.str(v.second.get<std::string>("diffuse"));
                buf >> kd;
                buf.clear();

                Vector3D ks;
                buf.str(v.second.get<std::string>("specular"));
                buf >> ks;
                buf.clear();

                double phongExp = 1.0;
                phongExp = v.second.get<double>("phongExp");

                double mirrorCoef = 1.0;
                buf.str(v.second.get<std::string>("mirrorCoef"));
                buf >> mirrorCoef;
                buf.clear();

                double roughness = 0;
                boost::optional<std::string> pRoughness = v.second.get_optional<
                        std::string>("roughness");
                if (pRoughness) {
                    buf.str(*pRoughness);
                    buf >> roughness;
                    buf.clear();
                }

                shader = new BlinnPhongMirrored(kd, ks, phongExp, mirrorCoef,
                                                roughness);
            } else if (type == "Dielectric") {
                double rI = v.second.get<double>("refractiveIndex");

                Vector3D attenCoef;
                boost::optional<std::string> pAttenCoef = v.second.get_optional<
                        std::string>("attenuationCoef");
                if (pAttenCoef) {
                    buf.str(*pAttenCoef);
                    buf >> attenCoef;
                    buf.clear();
                } else {
                    attenCoef = Vector3D(1, 1, 1);
                }

                shader = new Dielectric(rI, attenCoef);
            }

            // Add the shader to the map and return it
            if (shader == NULL) {
                shader = new NormalMap();
                std::cout << "Shader type " << type << " not implemented."
                << std::endl;
                std::cout << "Null shader converted to normal map shader."
                << std::endl;
            }
            shaders[name] = shader;
            return shader;
        } else {
            // optShaderRef is true, so this should be a shader reference.
            // Attempt to find the shader in the map and return it.

            // Assert that the shader is in the map
            assert(shaders.count(*optShaderRef) == 1);

            // std::cout << "Returning shader from map ..";
            // Note that name and type are undefined.
            return shaders[*optShaderRef];
        }
    }

    void Scene::parseCameraData(ptree::value_type const &v) {
        using std::string;
        std::istringstream buf;

        string name, type;
        Vector3D position, viewDir, lookatPoint;
        bool lookatSet = false;
        double focalLength;
        double imagePlaneWidth;

        name = v.second.get<string>("<xmlattr>.name");
        type = v.second.get<string>("<xmlattr>.type");

        buf.str(v.second.get<string>("position"));
        buf >> position;
        buf.clear();

        // LookAtPoint and ViewDir are optional, but one should be specified.
        boost::optional<string> plookatPoint = v.second.get_optional<string>(
                "lookatPoint");
        boost::optional<string> pviewDir = v.second.get_optional<string>("viewDir");

        if (plookatPoint) {
            lookatSet = true;
            buf.str(*plookatPoint);
            buf >> lookatPoint;
            buf.clear();
        } else if (pviewDir) {
            buf.str(*pviewDir);
            buf >> viewDir;
            buf.clear();
        }

        buf.str(v.second.get<string>("focalLength"));
        buf >> focalLength;
        buf.clear();

        buf.str(v.second.get<string>("imagePlaneWidth"));
        buf >> imagePlaneWidth;
        buf.clear();

        Camera* camera = NULL;
        if (lookatSet) {
            camera = new Camera(position, focalLength, imagePlaneWidth, nx, ny,
                                lookatPoint);
        } else {
            camera = new Camera(position, viewDir, focalLength, imagePlaneWidth, nx,
                                ny);
        }
        cameras.push_back(std::shared_ptr < Camera > (camera));
    }

    Matrix4x4 Scene::parseTransformData(ptree::value_type const &v) {
        using std::string;

        std::istringstream buf;
        ptree::const_iterator nodeIterator;

        Matrix4x4 transformation;
        transformation.setIdentity();

        for (nodeIterator = v.second.begin(); nodeIterator != v.second.end();
             ++nodeIterator) {
            if (nodeIterator->first == "translate") {
                Vector3D trans;
                buf.str(nodeIterator->second.get_value<string>());
                buf >> trans;
                buf.clear();

                Matrix4x4 translation;
                translation.makeTranslation(trans[0], trans[1], trans[2]);

                transformation = transformation * translation;
            } else if (nodeIterator->first == "rotate") {

                double degrees;
                buf.str(nodeIterator->second.get_value<string>());
                buf >> degrees;
                buf.clear();

                std::string axis = nodeIterator->second.get<string>(
                        "<xmlattr>.axis");

                double radians = M_PI / 180.0 * degrees;

                Matrix4x4 rotation;
                if (axis == "X") {
                    rotation.makeRotationX(radians);
                } else if (axis == "Y") {
                    rotation.makeRotationY(radians);
                } else if (axis == "Z") {
                    rotation.makeRotationZ(radians);
                }

                transformation = transformation * rotation;
            } else if (nodeIterator->first == "scale") {
                sivelab::Vector3D scale;
                buf.str(nodeIterator->second.get_value<string>());
                buf >> scale;
                buf.clear();

                Matrix4x4 scaleMatrix;
                scaleMatrix.makeScale(scale[0], scale[1], scale[2]);

                transformation = transformation * scaleMatrix;
            } else {
                // The last string is "<xmlattr>", which we can ignore
            }
        }
        return transformation;
    }

    Shape* Scene::loadMesh(const std::string& filename) {
        // Try to construct the directory from the current scene's filename
        std::string directory = this->filename.substr(0,
                                                      this->filename.find_last_of("\\/"));
        std::string mesh_file = directory + "/" + filename;

        std::cout << "Loading: " << mesh_file << std::endl;

        // Load the mesh
        ModelOBJ mOBJ;
        if (!mOBJ.import(mesh_file.c_str())) {
            std::cerr << "...failed to load mesh, exiting." << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cout << "Number of meshes contained within OBJ: "
        << mOBJ.getNumberOfMeshes() << std::endl;
        std::cout << "Number of triangles contained within OBJ: "
        << mOBJ.getNumberOfTriangles() << std::endl;

        const ModelOBJ::Mesh* pMesh = NULL;
        const ModelOBJ::Vertex* pVertices = NULL;

        const int *idxBuffer = mOBJ.getIndexBuffer();
        for (int mIdx = 0; mIdx < mOBJ.getNumberOfMeshes(); mIdx++) {
            std::vector<Shape*> triangles;

            pMesh = &mOBJ.getMesh(mIdx);
            pVertices = mOBJ.getVertexBuffer();

            for (int i = pMesh->startIndex;
                 i < (pMesh->startIndex + pMesh->triangleCount * 3); i += 3) {
                ModelOBJ::Vertex v0, v1, v2;
                v0 = pVertices[idxBuffer[i]];
                v1 = pVertices[idxBuffer[i + 1]];
                v2 = pVertices[idxBuffer[i + 2]];

                Triangle* triangle = new Triangle(
                        Vertex(
                                Vector3D(v0.position[0], v0.position[1],v0.position[2]),
                                Vector3D(v0.normal[0], v0.normal[1],v0.normal[2]),
                                Vector3D(),
                                v0.texCoord[0],
                                v0.texCoord[1]
                        ),
                        Vertex(
                                Vector3D(v1.position[0], v1.position[1],v1.position[2]),
                                Vector3D(v1.normal[0], v1.normal[1],v1.normal[2]),
                                Vector3D(),
                                v1.texCoord[0],
                                v1.texCoord[1]
                        ),Vertex(
                                Vector3D(v2.position[0], v2.position[1],v2.position[2]),
                                Vector3D(v2.normal[0], v2.normal[1],v2.normal[2]),
                                Vector3D(),
                                v2.texCoord[0],
                                v2.texCoord[1]
                        )
                );
                triangles.push_back(triangle);
            }

            Mesh* mesh = new Mesh(triangles);
            return mesh;
        }

        std::cerr << "Couldn't load mesh." << std::endl;
        exit(EXIT_FAILURE);
    }
}
