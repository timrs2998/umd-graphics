#pragma once

#include <Vector3D.h>

namespace sivelab {

    class Ray {
    public:
        Ray(Vector3D origin, Vector3D direction);
        virtual ~Ray();
        const Vector3D& getDirection(void) const;
        const Vector3D& getOrigin(void) const;
        void setDirection(const Vector3D& direction);
        void setOrigin(const Vector3D& origin);

    private:
        Vector3D origin;
        Vector3D direction;
    };
}
