#pragma once

#include <Vector3D.h>

namespace sivelab {
    class Ray;

    class BBox {
    public:
        BBox();
        BBox(Vector3D minPoint, Vector3D maxPoint);
        virtual ~BBox();
        bool intersects(const Ray& ray, const double tmin, const double tmax) const;
        const Vector3D& getMaxPoint() const;
        const Vector3D& getMinPoint() const;

    private:
        Vector3D minPoint, maxPoint;
    };
}
