#include <Dielectric.h>
#include <cmath>
#include <limits>

#include "../HitStruct.h"
#include "../Ray.h"
#include "../Scene.h"
#include "../lights/Light.h"

namespace sivelab {

    Dielectric::Dielectric(double rI, Vector3D attenCoef) :
            Mirror(), rI(rI), attenCoef(attenCoef) {
    }

    Dielectric::~Dielectric() {
    }

    /** No shadows */
    Vector3D Dielectric::apply(const HitStruct& hit, const Scene& scene,
                               int depth) const {
        Light* light = new Light(Vector3D(), Vector3D());
        return getComponent(hit, scene, *light, depth);
    }

    /** Finds the reflection vector given the incidence and normal */
    inline Vector3D reflect(const Vector3D& d, const Vector3D& n) {
        return d - 2 * (d.dot(n)) * n;		// Eq 4.5 on page 87
    }

    /** False if total internal reflection otherwise sets t refraction ray's direction */
    inline bool refract(const Vector3D& d, const Vector3D& normal, double n,
                        Vector3D& t) {
        double n1 = 1;
        double n2 = n;

        double underRoot = 1
                           - (n1 * n1) * (1 - d.dot(normal) * d.dot(normal)) / (n2 * n2);

        // Total internal reflection occurs when square root is negative
        if (underRoot < 0)
            return false;

        t = n1 * (d - normal * d.dot(normal)) / n2 - normal * sqrt(underRoot);
        return true;
    }

    /* Page 307 */
    Vector3D Dielectric::getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const {
        double tmin = 1e-3;
        double tmax = std::numeric_limits<double>::max();

        Vector3D d = -1 * hit.v;						// d is the incidence ray
        Vector3D p = hit.intersection;					// p is point on dielectric surface
        const double n = rI;							// refractive index of object
        Vector3D t;
        Vector3D k(1, 1, 1);
        double cos;										// cos theta = ...

        Vector3D colorTransmittence;
        Vector3D colorReflective = Mirror::getComponent(hit, scene, light, depth);

        Vector3D r = reflect(d, hit.n);					// reflection ray
        if (d.dot(hit.n) < 0) {							// Case 1: going into medium
            refract(d, hit.n, n, t);
            cos = -d.dot(hit.n);
        } else { 										// Case 2: going out of medium
            k[0] = exp(-log(attenCoef[0]) * hit.t);
            k[1] = exp(-log(attenCoef[1]) * hit.t);
            k[2] = exp(-log(attenCoef[2]) * hit.t);

            if (refract(d, -1 * hit.n, 1 / n, t)) {
                cos = t.dot(hit.n);
            } else {
                return k * colorReflective;
            }
        }
        colorTransmittence = scene.getColor(Ray(p, t), tmin, tmax, depth);
        double R0 = pow(n - 1, 2) / pow(n + 1, 2); // Reflectance at normal incidence
        double R = R0 + (1 - R0) * pow(1 - cos, 5); 	// Reflectance
        return k * (R * colorReflective + (1 - R) * colorTransmittence);
    }

}
