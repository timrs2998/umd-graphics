#pragma once

#include <BlinnPhong.h>
#include <Mirror.h>

namespace sivelab {
struct HitStruct;
class Light;
class Scene;

    class BlinnPhongMirrored: virtual public BlinnPhong, virtual public Mirror {
    public:
        BlinnPhongMirrored(Vector3D kd, Vector3D ks, double phongExp, double km);
        BlinnPhongMirrored(Vector3D kd, Vector3D ks, double phongExp, double km,
                           double roughness);
        virtual ~BlinnPhongMirrored();

        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;

    private:
        const double roughness;
        const double km;
    };

}
