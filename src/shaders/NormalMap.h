#pragma once

#include <Shader.h>

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;

    class NormalMap: public Shader {
    public:
        NormalMap();
        virtual ~NormalMap();

        virtual Vector3D apply(const HitStruct &hit, const Scene& scene,
                               int depth) const override;

        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;
    };

}
