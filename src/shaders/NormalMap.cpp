#include <NormalMap.h>
#include <Vector3D.h>
#include <vector>

#include "../HitStruct.h"
#include "../lights/Light.h"
#include "../Scene.h"

namespace sivelab {

    NormalMap::NormalMap() {
    }

    NormalMap::~NormalMap() {
    }

    Vector3D NormalMap::apply(const HitStruct& hit, const Scene& scene,
                              int depth) const {
        Light* light = scene.getLights().front();
        return getComponent(hit, scene, *light, depth);
    }

    Vector3D NormalMap::getComponent(const HitStruct& hit, const Scene& scene,
                                     const Light& light, int depth) const {
        return hit.n * 0.5 + Vector3D(.5, .5, .5);
    }

}

