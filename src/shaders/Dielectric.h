#pragma once

#include <Mirror.h>
#include <Vector3D.h>

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;

    class Dielectric: public Mirror {
    public:
        Dielectric(double rI, Vector3D attenCoef);
        virtual ~Dielectric();

        virtual Vector3D apply(const HitStruct &hit, const Scene& scene,
                               int depth) const override;
        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;
    private:
        const double rI;
        const Vector3D attenCoef;
    };

}
