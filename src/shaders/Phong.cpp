#include <Phong.h>
#include <cmath>

#include "../HitStruct.h"
#include "../lights/Light.h"

namespace sivelab {

    Phong::Phong(Vector3D kd, Vector3D ks, double phongExp) :
            Lambertian(kd), ks(ks), phongExp(phongExp) {
    }

    Phong::~Phong() {
    }

    Vector3D Phong::getComponent(const HitStruct& hit, const Scene& scene,
                                 const Light& light, int depth) const {
        Vector3D color = Lambertian::getComponent(hit, scene, light, depth);

        Vector3D li = light.getIntensity();
        Vector3D l = light.getPosition() - hit.intersection;
        l.normalize();

        Vector3D r = -1 * l + 2 * (l.dot(hit.n)) * hit.n;
        r.normalize();

        color += ks * li * pow(fmax(0, hit.v.dot(r)), phongExp);
        return color;
    }

}
