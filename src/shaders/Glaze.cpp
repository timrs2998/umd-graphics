#include <Glaze.h>
#include <Vector3D.h>

namespace sivelab {

    Glaze::Glaze(Vector3D kd, double km) :
            Lambertian(kd), km(km) {
    }

    Glaze::~Glaze() {
    }

    Vector3D Glaze::getComponent(const HitStruct& hit, const Scene& scene,
                                 const Light& light, int depth) const {
        return Lambertian::getComponent(hit, scene, light, depth)
               + km * Mirror::getComponent(hit, scene, light, depth);
    }

}
