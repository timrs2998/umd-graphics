#include <Lambertian.h>
#include <cmath>

#include "../textures/Texture.h"
#include "../HitStruct.h"
#include "../lights/Light.h"
#include "../shapes/Shape.h"

namespace sivelab {

    Lambertian::Lambertian(Vector3D kd) :
            kd(kd), texture(NULL) {
    }

    Lambertian::Lambertian(Texture* texture) :
            texture(texture) {
    }

    Lambertian::~Lambertian() {
    }

    Vector3D Lambertian::getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const {
        Vector3D li = light.getIntensity();
        Vector3D l = light.getPosition() - hit.intersection;
        l.normalize();

        return getDiffuse(hit) * li * fmax(0, hit.n.dot(l));
    }

    Vector3D Lambertian::getDiffuse(const HitStruct& hit) const {
        if (texture != NULL) {
            double s = hit.shape->getS(hit.intersection);
            double t = hit.shape->getT(hit.intersection);
            return texture->lookup(s, t);
        }
        return kd;
    }

}
