#pragma once

#include <Lambertian.h>
#include <Vector3D.h>

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;

    class BlinnPhong: virtual public Lambertian {
    public:
        BlinnPhong(Vector3D kd, Vector3D ks, double phongExp);
        virtual ~BlinnPhong();

        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;
    private:
        const Vector3D ks;
        const double phongExp;
    };

}
