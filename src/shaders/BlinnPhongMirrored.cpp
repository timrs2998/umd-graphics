#include <BlinnPhongMirrored.h>
#include <limits>
#include "../Ray.h"
#include "../Basis.h"
#include "../HitStruct.h"
#include "../Scene.h"

namespace sivelab {

    BlinnPhongMirrored::BlinnPhongMirrored(Vector3D kd, Vector3D ks,
                                           double phongExp, double km) :
            Lambertian(kd), BlinnPhong(kd, ks, phongExp), roughness(0), km(km) {
    }

    BlinnPhongMirrored::BlinnPhongMirrored(Vector3D kd, Vector3D ks,
                                           double phongExp, double km, double roughness) :
            Lambertian(kd), BlinnPhong(kd, ks, phongExp), roughness(roughness),
            km(km) {
    }

    BlinnPhongMirrored::~BlinnPhongMirrored() {
    }

    Vector3D BlinnPhongMirrored::getComponent(const HitStruct& hit,
                                              const Scene& scene, const Light& light, int depth) const {
        Vector3D color;
        if (roughness == 0) {
            // Ideal specular reflection
            color = (1.0 - km) * BlinnPhong::getComponent(hit, scene, light, depth)
                    + km * Mirror::getComponent(hit, scene, light, depth);
        } else {
            // Surface has roughness specified
            Vector3D rd = -1 * hit.v;
            Vector3D r = rd - 2 * (rd.dot(hit.n)) * hit.n;

            Basis basis(r);
            double u = -roughness / 2.0 + drand48() * roughness;
            double v = -roughness / 2.0 + drand48() * roughness;

            Vector3D rp = r + u * basis.getU() + v * basis.getV();

            // Send our recursive reflection ray, rp
            Ray ray(hit.intersection, rp);
            const double tmin = 1e-3;
            double tmax = std::numeric_limits<double>::max();
            color = (1.0 - km) * BlinnPhong::getComponent(hit, scene, light, depth)
                    + km * scene.getColor(ray, tmin, tmax, depth);
        }
        return color;
    }

}
