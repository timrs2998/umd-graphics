#pragma once

#include <Shader.h>

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;

    class Mirror: virtual public Shader {
    public:
        Mirror();
        virtual ~Mirror();

        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;
    };

}
