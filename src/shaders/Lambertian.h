#pragma once

#include <Shader.h>
#include <Vector3D.h>

namespace sivelab {
    class Texture;

    class Lambertian: virtual public Shader {
    public:
        Lambertian(Vector3D kd);
        Lambertian(Texture* texture);

        virtual ~Lambertian();
        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;

    protected:
        Vector3D getDiffuse(const HitStruct& hit) const;

    private:
        const Vector3D kd;
        const Texture* texture;
    };

}
