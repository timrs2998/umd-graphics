#pragma once

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;
    class Vector3D;

    class Shader {
    public:
        Shader();
        virtual ~Shader();
        virtual Vector3D apply(const HitStruct &hit, const Scene& scene,
                               int depth) const;
        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const = 0;

    protected:
        bool inShadow(const Vector3D& intersection, const Vector3D& light,
                      const Scene& scene) const;
    };

}
