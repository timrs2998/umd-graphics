#pragma once

#include <Lambertian.h>
#include <Mirror.h>

namespace sivelab {
    struct HitStruct;
    class Light;
    class Scene;

    class Glaze: virtual public Lambertian, virtual public Mirror {
    public:
        Glaze(Vector3D kd, double km);
        virtual ~Glaze();

        virtual Vector3D getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const override;

    private:
        const double km;
    };

}
