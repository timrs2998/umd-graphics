#include <BlinnPhong.h>
#include <cmath>

#include "../HitStruct.h"
#include "../lights/Light.h"

namespace sivelab {

    BlinnPhong::BlinnPhong(Vector3D kd, Vector3D ks, double phongExp) :
            Lambertian(kd), ks(ks), phongExp(phongExp) {
    }

    BlinnPhong::~BlinnPhong() {
    }

    Vector3D BlinnPhong::getComponent(const HitStruct& hit, const Scene& scene,
                                      const Light& light, int depth) const {
        Vector3D color = Lambertian::getComponent(hit, scene, light, depth);

        Vector3D li = light.getIntensity();
        Vector3D l = light.getPosition() - hit.intersection;
        l.normalize();

        Vector3D h = hit.v + l;
        h.normalize();

        color += ks * li * pow(fmax(0, hit.n.dot(h)), phongExp);
        return color;
    }

}
