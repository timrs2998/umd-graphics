#include <Shader.h>
#include <Vector3D.h>
#include <vector>

#include "../lights/Area.h"
#include "../HitStruct.h"
#include "../Ray.h"
#include "../Scene.h"

namespace sivelab {

    Shader::Shader() {
    }

    Shader::~Shader() {
    }

    Vector3D Shader::apply(const HitStruct& hit, const Scene& scene,
                           int depth) const {
        Vector3D color;

        std::vector<Light*> lights = scene.getLights();
        for (auto* light : lights) {
            if (inShadow(hit.intersection, light->getPosition(), scene) == false) {
                color += getComponent(hit, scene, *light, depth);
            }
        }

        // Approximate soft shadows
        std::vector<Area*> areaLights = scene.getAreaLights();
        for (auto* area : areaLights) {
            Vector3D areaColor;
            const int N = area->getN();

            for (int p = 0; p < N; p++) {
                for (int q = 0; q < N; q++) {
                    Light point(area->getPosition(p + drand48(), q + drand48()),
                                area->getIntensity());
                    if (inShadow(hit.intersection, point.getPosition(), scene)
                        == false) {
                        areaColor += getComponent(hit, scene, point, depth);
                    }
                }
            }
            areaColor /= (N * N);
            color += areaColor;
        }

        return color.clamp(0, 1);
    }

    bool Shader::inShadow(const Vector3D& intersection, const Vector3D& light,
                          const Scene& scene) const {
        Ray shadowRay(intersection, light - intersection);
        const double tmin = 1e-4;
        double tmax = 1;			// 1 is the full distance b/w the ip and the light
        return scene.anyHit(shadowRay, tmin, tmax);
    }

}
