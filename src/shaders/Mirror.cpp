#include <Mirror.h>
#include <Vector3D.h>
#include <limits>

#include "../HitStruct.h"
#include "../Ray.h"
#include "../Scene.h"

namespace sivelab {
    class Light;
}

namespace sivelab {

    Mirror::Mirror() {
    }

    Mirror::~Mirror() {
    }

    Vector3D Mirror::getComponent(const HitStruct& hit, const Scene& scene,
                                  const Light& light, int depth) const {
        Ray ray(hit.intersection, -1 * hit.v + 2 * (hit.v.dot(hit.n)) * hit.n);
        const double tmin = 1e-3;
        double tmax = std::numeric_limits<double>::max();
        return scene.getColor(ray, tmin, tmax, depth);
    }

}
