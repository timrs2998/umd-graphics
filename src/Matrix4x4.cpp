#include "Matrix4x4.h"
#include <iostream>
#include <cassert>
#include <vector>
#include <cmath>
#include <Vector3D.h>

namespace sivelab {

    Matrix3x3::Matrix3x3() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                d[i][j] = 0;
            }
        }
    }

    Matrix3x3::Matrix3x3(const double a11, const double a12, const double a13,
                         const double a21, const double a22, const double a23, const double a31,
                         const double a32, const double a33) :
            d { { a11, a12, a13 }, { a21, a22, a23 }, { a31, a32, a33 } } {
    }

    void Matrix3x3::set(const double a11, const double a12, const double a13,
                        const double a21, const double a22, const double a23, const double a31,
                        const double a32, const double a33) {
        d[0][0] = a11;
        d[0][1] = a12;
        d[0][2] = a13;

        d[1][0] = a21;
        d[1][1] = a22;
        d[1][2] = a23;

        d[2][0] = a31;
        d[2][1] = a32;
        d[2][2] = a33;
    }

    double Matrix3x3::det(void) const {
        return d[0][0] * (d[1][1] * d[2][2] - d[1][2] * d[2][1])
               - d[0][1] * (d[1][0] * d[2][2] - d[1][2] * d[2][0])
               + d[0][2] * (d[1][0] * d[2][1] - d[1][1] * d[2][0]);
    }

    Matrix4x4::Matrix4x4() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                d[i][j] = 0;
            }
        }
    }

    Matrix4x4::Matrix4x4(const double a11, const double a12, const double a13,
                         const double a14, const double a21, const double a22, const double a23,
                         const double a24, const double a31, const double a32, const double a33,
                         const double a34, const double a41, const double a42, const double a43,
                         const double a44) :
            d { { a11, a12, a13, a14 }, { a21, a22, a23, a24 },
                { a31, a32, a33, a34 }, { a41, a42, a43, a44 } } {
    }

    void Matrix4x4::set(const double a11, const double a12, const double a13,
                        const double a14, const double a21, const double a22, const double a23,
                        const double a24, const double a31, const double a32, const double a33,
                        const double a34, const double a41, const double a42, const double a43,
                        const double a44) {
        d[0][0] = a11;
        d[0][1] = a12;
        d[0][2] = a13;
        d[0][3] = a14;

        d[1][0] = a21;
        d[1][1] = a22;
        d[1][2] = a23;
        d[1][3] = a24;

        d[2][0] = a31;
        d[2][1] = a32;
        d[2][2] = a33;
        d[2][3] = a34;

        d[3][0] = a41;
        d[3][1] = a42;
        d[3][2] = a43;
        d[3][3] = a44;
    }

    void Matrix4x4::print() {
        using std::cout;
        using std::endl;

        for (int i = 0; i < n; i++) {
            cout << "| ";
            for (int j = 0; j < n; j++) {
                cout << d[i][j] << " ";
            }
            cout << "|" << endl;
        }
    }

    void Matrix4x4::setIdentity(void) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    d[i][j] = 1;
                } else {
                    d[i][j] = 0;
                }
            }
        }
    }

    const Matrix4x4 Matrix4x4::operator*(const Matrix4x4& rhs) const {
        Matrix4x4 product;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                product.d[i][j] = 0;
                for (int m = 0; m < n; m++) {
                    product.d[i][j] += d[i][m] * rhs.d[m][j];
                }
            }
        }
        return product;
    }

    const Vector3D Matrix4x4::operator*(const Vector3D& rhs) const {
        // Determines y = A x where this is A
        double x[] = {rhs[0], rhs[1], rhs[2], 1};
        double y[] = {0, 0, 0, 0};

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                y[i] += d[i][j] * x[j];
            }
        }
        double wp = y[3];
        return Vector3D(y[0] / wp, y[1] / wp, y[2] / wp);
    }

    const Vector3D Matrix4x4::multVector(const Vector3D& rhs,
                                         const double w) const {
        // The vector either represents a position (1) or a direction (0)
        assert(w == 0 || w == 1);

        // Determines y = A x where this is A
        double x[] = {rhs[0], rhs[1], rhs[2], w};
        double y[] = {0, 0, 0, 0};

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                y[i] += d[i][j] * x[j];
            }
        }
        return Vector3D(y[0], y[1], y[2]);
    }

    double Matrix4x4::det(void) const {
        const int i = 0;

        // Calculate determinant by expanding across the first row
        double determinant = 0;
        for (int j = 0; j < n; j++) {
            determinant += pow(-1, i + j) * d[i][j] * cofactor(i, j);
        }
        return determinant;
    }

    double Matrix4x4::cofactor(int i, int j) const {
        std::vector<double> values;
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < n; y++) {
                if (x != i && y != j) {
                    values.push_back(d[x][y]);
                }
            }
        }

        Matrix3x3 minor;
        minor.set(
                values.at(0), values.at(1), values.at(2),
                values.at(3), values.at(4), values.at(5),
                values.at(6), values.at(7),	values.at(8)
        );

        return minor.det();
    }

    const Matrix4x4 Matrix4x4::inverse(void) const {
        Matrix4x4 inverse;

        // A^-1 = 1 / det(A) * Transpose(Cofactor)
        double determinant = det();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                inverse.d[i][j] = pow(-1, i + j) * cofactor(j, i) / determinant;
            }
        }
        return inverse;
    }

    const Matrix4x4 Matrix4x4::transpose(void) const {
        Matrix4x4 transpose;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                transpose.d[j][i] = d[i][j];
            }
        }
        return transpose;
    }

    void Matrix4x4::makeTranslation(const double x, const double y,
                                    const double z) {
        set(1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1);

    }
    void Matrix4x4::makeScale(const double x, const double y, const double z) {
        set(x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1);
    }

    void Matrix4x4::makeRotationX(const double angle_in_radians) {
        set(1, 0, 0, 0, 0, cos(angle_in_radians), -sin(angle_in_radians), 0, 0,
            sin(angle_in_radians), cos(angle_in_radians), 0, 0, 0, 0, 1);
    }

    void Matrix4x4::makeRotationY(const double angle_in_radians) {
        set(cos(angle_in_radians), 0, sin(angle_in_radians), 0, 0, 1, 0, 0,
            -sin(angle_in_radians), 0, cos(angle_in_radians), 0, 0, 0, 0, 1);
    }

    void Matrix4x4::makeRotationZ(const double angle_in_radians) {
        d[0][0] = cos(angle_in_radians);
        d[0][1] = -sin(angle_in_radians);
        d[0][2] = 0;
        d[0][3] = 0;

        d[1][0] = sin(angle_in_radians);
        d[1][1] = cos(angle_in_radians);
        d[1][2] = 0;
        d[1][3] = 0;

        d[2][0] = 0;
        d[2][1] = 0;
        d[2][2] = 1;
        d[2][3] = 0;

        d[3][0] = 0;
        d[3][1] = 0;
        d[3][2] = 0;
        d[3][3] = 1;
    }
}
