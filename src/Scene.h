#pragma once

#include <boost/property_tree/ptree_fwd.hpp>
#include <SceneDataContainer.h>
#include <Shader.h>
#include <Shape.h>
#include <Vector3D.h>
#include <XMLSceneParser.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include "png++/png.hpp"

#include "Camera.h"
#include "lights/Light.h"

namespace sivelab {
    class Area;
    class BvhNode;
    class Camera;
    class Light;
    class Shader;
    class Shape;
    class Matrix4x4;
    class Texture;
    class Ray;
    struct ViewFrustum;

    class Scene: public SceneElementCreator {
    public:
        Scene(std::string filename, int nx, int ny, int N);
        virtual ~Scene();

        // Methods for parsing xml data and creating object instances
        Shape* loadMesh(const std::string& filename);
        void parseSceneData(ptree::value_type const &v);
        void parseShapeData(ptree::value_type const &v);
        Shader* parseShaderData(ptree::value_type const &v);
        void parseTextureData(ptree::value_type const &v);
        Matrix4x4 parseTransformData(ptree::value_type const &v);
        void parseCameraData(ptree::value_type const &v);
        void parseLightData(ptree::value_type const &v);
        void instance(ptree::value_type const &value);

        // Methods for raytracing
        Vector3D getColor(const Ray& ray, const double tmin, double& tmax,
                          int depth) const;

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& h) const;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const;

        // Methods for rasterizing
        png::image<png::rgb_pixel> rasterize() const;
        void hwRasterize() const;

        // Getters
        const std::vector<std::shared_ptr<Camera>>& getCameras(void) const;
        const std::vector<Light*>& getLights(void) const;
        const std::vector<Area*>& getAreaLights(void) const;

    private:
        std::vector<RasterTriangle> shapesToTriangles(
                const ViewFrustum& viewFrustum, const Vector3D& position) const;

        XMLSceneParser xmlScene;
        const std::string filename;
        const int nx, ny, N;
        std::vector<std::shared_ptr<Camera>> cameras;

        std::vector<Light*> lights;
        std::vector<Area*> areaLights;
        std::vector<Shape*> shapes;

        std::map<std::string, Shape*> instanceMap;
        std::map<std::string, Texture*> textureMap;
        std::map<std::string, Shader*> shaders;
        Vector3D bgColor;
        BvhNode* root;
    };
}
