#include "BBox.h"
#include "Ray.h"

namespace sivelab {

    BBox::BBox() {
    }

    BBox::BBox(Vector3D minPoint, Vector3D maxPoint) :
            minPoint(minPoint), maxPoint(maxPoint) {
    }

    BBox::~BBox() {
    }

    bool BBox::intersects(const Ray& ray, const double tmin,
                          const double tmax) const {
        Vector3D rd = ray.getDirection();
        Vector3D ro = ray.getOrigin();

        double txmin, txmax;
        if (rd[0] >= 0) {
            txmin = (minPoint[0] - ro[0]) / rd[0];
            txmax = (maxPoint[0] - ro[0]) / rd[0];
        } else {
            txmax = (minPoint[0] - ro[0]) / rd[0];
            txmin = (maxPoint[0] - ro[0]) / rd[0];
        }

        double tymin, tymax;
        if (rd[1] >= 0) {
            tymin = (minPoint[1] - ro[1]) / rd[1];
            tymax = (maxPoint[1] - ro[1]) / rd[1];
        } else {
            tymax = (minPoint[1] - ro[1]) / rd[1];
            tymin = (maxPoint[1] - ro[1]) / rd[1];
        }

        double tzmin, tzmax;
        if (rd[2] >= 0) {
            tzmin = (minPoint[2] - ro[2]) / rd[2];
            tzmax = (maxPoint[2] - ro[2]) / rd[2];
        } else {
            tzmax = (minPoint[2] - ro[2]) / rd[2];
            tzmin = (maxPoint[2] - ro[2]) / rd[2];
        }

        // Check for intersection
        if (txmin > tymax || tymin > txmax)
            return false;
        else if (txmin > txmax || tzmin > txmax)
            return false;
        else if (tymin > tzmax || tzmin > tzmax)
            return false;
        return true;
    }

    const Vector3D& BBox::getMaxPoint() const {
        return maxPoint;
    }

    const Vector3D& BBox::getMinPoint() const {
        return minPoint;
    }
}
