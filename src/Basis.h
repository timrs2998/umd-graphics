#pragma once

#include <Vector3D.h>

namespace sivelab {

    class Basis {
    public:
        Basis();
        Basis(Vector3D a);
        Basis(Vector3D a, Vector3D b);
        virtual ~Basis();

        const Vector3D& getU() const;
        const Vector3D& getV() const;
        const Vector3D& getW() const;

    private:
        void init(Vector3D a, Vector3D b);
        bool areColinear(const Vector3D& a, const Vector3D& b) const;
        Vector3D u, v, w;
    };
}
