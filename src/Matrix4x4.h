#pragma once

namespace sivelab {
    class Vector3D;

    class Matrix3x3 {
    public:
        double d[3][3];

        /** Creates the zero matrix */
        Matrix3x3();

        Matrix3x3(const double a11, const double a12, const double a13,
                  const double a21, const double a22, const double a23,
                  const double a31, const double a32, const double a33);

        void set(const double a11, const double a12, const double a13,
                 const double a21, const double a22, const double a23,
                 const double a31, const double a32, const double a33);

        double det(void) const;
    private:
        int n = 3;
    };

    class Matrix4x4 {
    public:
        double d[4][4];

        /** Creates the zero matrix */
        Matrix4x4();

        Matrix4x4(const double a11, const double a12, const double a13,
                  const double a14, const double a21, const double a22,
                  const double a23, const double a24, const double a31,
                  const double a32, const double a33, const double a34,
                  const double a41, const double a42, const double a43,
                  const double a44);

        void set(const double, const double, const double, const double,
                 const double, const double, const double, const double,
                 const double, const double, const double, const double,
                 const double, const double, const double, const double);

        void print();

        void setIdentity(void);

        const Matrix4x4 operator*(const Matrix4x4&) const;
        const Vector3D operator*(const Vector3D&) const;
        /** Multiplies a matrix by the x, y, z coords of the vector and the given w value */
        const Vector3D multVector(const Vector3D& rhs, const double w) const;
        const double multVectorForW(const Vector3D& rhs, const double w) const;

        double det(void) const;
        double cofactor(int i, int j) const;
        const Matrix4x4 inverse(void) const;
        const Matrix4x4 transpose(void) const;       // return the transpose of self

        void makeTranslation(const double x, const double y, const double z);
        void makeScale(const double x, const double y, const double z);

        void makeRotationX(const double angle_in_radians);
        void makeRotationY(const double angle_in_radians);
        void makeRotationZ(const double angle_in_radians);

    private:
        int n = 4;
    };

    inline const Matrix4x4 operator*(const Matrix4x4& m, const double c) {
        return Matrix4x4(m.d[0][0] * c, m.d[0][1] * c, m.d[0][2] * c, m.d[0][3] * c,
                         m.d[1][0] * c, m.d[1][1] * c, m.d[1][2] * c, m.d[1][3] * c,
                         m.d[2][0] * c, m.d[2][1] * c, m.d[2][2] * c, m.d[2][3] * c,
                         m.d[3][0] * c, m.d[3][1] * c, m.d[3][2] * c, m.d[3][3] * c);
    }
}
