#include "Texture.h"
#include <cmath>

namespace sivelab {

    Texture::Texture(Vector3D** colors, const int width, const int height)
            : colors(colors), width(width), height(height) {
    }

    Texture::~Texture() {
        // Delete colors
        for (int i = 0; i < width; i++) {
            delete[] colors[i];
        }
        delete[] colors;
    }

    Vector3D Texture::lookup(const double s, const double t) const {
        assert(0 <= s && s <= 1);
        assert(0 <= t && t <= 1);

        // Conversion to pixel coordinates
        int i = (int) floor(s * width) % width;
        int j = (int) floor(t * height) % height;
        return colors[i][j];
    }
}
