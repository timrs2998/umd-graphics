#pragma once

#include <Vector3D.h>
namespace sivelab {

    class Texture {
    public:
        Texture(Vector3D** colors, const int width, const int height);
        virtual ~Texture();

        /** Nearest neighbor lookup */
        virtual Vector3D lookup(const double s, const double t) const;

    private:
        Vector3D** colors;
        const int width, height;
    };
}
