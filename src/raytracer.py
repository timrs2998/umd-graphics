from math import *
import png

width = 500
height = 500
rpp = 1
depth = 3


class Vector:
    """
    Python port of Pete's Vector3D class from C++
    """
    def __init__(self, a, b, c):
        self.data = [float(a), float(b), float(c)]

    @classmethod
    def from_zero(cls):
        return cls(0, 0, 0)

    @classmethod
    def from_data(cls, data):
        return cls(data[0], data[1], data[2])

    def dot(self, other):
        return self.data[0] * other.data[0] + self.data[1] * other.data[1] + self.data[2] * other.data[2]

    def cross(self, other):
        a = self.data
        b = other.data
        c = list()
        c.append(a[1]*b[2] - a[2]*b[1])
        c.append(a[2]*b[0] - a[0]*b[2])
        c.append(a[0]*b[1] - a[1]*b[0])
        return Vector.from_data(c)

    def normalize(self):
        mag = float(self.length())
        if mag > 0:
            self.data[0] /= mag
            self.data[1] /= mag
            self.data[2] /= mag

    def length(self):
        return sqrt(pow(self.data[0], 2) + pow(self.data[1], 2) + pow(self.data[2], 2))

    def clamp(self, minimum = 0, maximum = 1):
        for i in range(0, 3):
            if self.data[i] > maximum:
                self.data[i] = float(maximum)
            elif self.data[i] < minimum:
                self.data[i] = float(minimum)

    def __mul__(self, other):
        if isinstance(other, Vector):
            data = list()
            for i in range(0, 3):
                data.append(self.data[i] * other.data[i])
            return Vector.from_data(data)
        else:
            data = [x * other for x in self.data]
            return Vector.from_data(data)

    def __add__(self, other):
        data = []
        for i in range(0, 3):
            data.append(self.data[i] + other.data[i])
        return Vector.from_data(data)

    def __sub__(self, other):
        data = []
        for i in range(0, 3):
            data.append(self.data[i] - other.data[i])
        return Vector.from_data(data)

    __rmul__ = __mul__
    __radd__ = __add__
    __rsub__ = __sub__


def are_colinear(a, b):
    a_norm = (a * (1 / a.length())).data
    b_norm = (b * (1 / b.length())).data
    epsilon = 1e-3
    return abs(a_norm[0] - b_norm[0]) < epsilon \
        and abs(a_norm[1] - b_norm[1]) < epsilon \
        and abs(a_norm[2] - b_norm[2]) < epsilon


def in_shadow(point, light_vector, scene):
    shadow_ray = Ray(point, light_vector - point)
    tmin = 1e-4
    tmax = 1
    hit = Hit()
    return scene.intersects(shadow_ray, tmin, tmax, hit)


class Hit:
    def __init__(self):
        pass


class Ray:
    def __init__(self, origin, direction):
        self.origin = origin
        self.direction = direction


class Sphere:
    def __init__(self, center, radius):
        self.center = center
        self.radius = float(radius)
        self.shader = None

    def intersects(self, ray, tmin, tmax, hit):
        ro = ray.origin
        rd = ray.direction

        a = rd.dot(rd)
        b = (2.0 * rd).dot(ro - self.center)
        c = (ro - self.center).dot(ro - self.center) - self.radius * self.radius

        discriminant = b * b - 4.0 * a * c
        if discriminant < 0.0:
            return False

        t0 = (-b + sqrt(discriminant)) / (2.0 * a)
        t1 = (-b - sqrt(discriminant)) / (2.0 * a)
        t = t0 if t0 < t1 else t1

        if tmin < t < tmax:
            hit.intersection = ro + t * rd
            hit.n = hit.intersection - self.center
            hit.v = ro - hit.intersection
            hit.n.normalize()
            hit.v.normalize()
            hit.shader = self.shader
            tmax = t        # TODO: tmax isn't passed by reference
            return True
        return False


class Light:
    def __init__(self, position, intensity):
        self.position = position
        self.intensity = intensity


class Shader:
    def apply(self, hit, scene, depth):
        color = Vector.from_zero()
        for light in scene.lights:
            if not in_shadow(hit.intersection, light.position, scene):
                color += self.get_component(hit, scene, light, depth)
        return color


class Lambertian(Shader):
    def __init__(self, kd):
        self.kd = kd

    def get_component(self, hit, scene, light, depth):
        l = light.position - hit.intersection
        l.normalize()
        return self.kd * light.intensity * max(0, hit.n.dot(l))


class BlinnPhong(Lambertian):
    def __init__(self, kd, ks, phong_exp):
        self.kd = kd
        self.ks = ks
        self.phong_exp = phong_exp

    def get_component(self, hit, scene, light, depth):
        li = light.intensity

        l = light.position - hit.intersection
        l.normalize()

        h = hit.v + l
        h.normalize()

        color = Lambertian.get_component(self, hit, scene, light, depth)
        return color + self.ks * li * pow(max(0, hit.n.dot(h)), self.phong_exp)


class Camera:
    def __init__(self, position, view_dir, f_len, img_pln_width, nx, ny):
        self.position = position
        self.f_len = f_len
        self.img_pln_width = img_pln_width
        self.nx = float(nx)
        self.ny = float(ny)
        self.img_pln_height = self.nx / self.ny * img_pln_width
        self.construct_basis(view_dir, Vector.from_data([0, 1, 0]))

    def construct_basis(self, a, b):
        w = -1.0 * a
        w.normalize()

        u = w.cross(b)

        # Watch out for b colinear with w
        if are_colinear(w, b):
            epsilon = 1e-3
            u = w.cross(Vector.from_data([b[0] + epsilon, b[1] + epsilon, b[2] + epsilon]))

        u.normalize()

        v = w.cross(u)
        v.normalize()
        u = -1.0 * u
        self.w = w; self.v = u; self.u = v

    def compute_ray(self, x, y):
        l = -self.img_pln_width / 2.0
        r = self.img_pln_width / 2.0
        b = -self.img_pln_height / 2.0
        t = self.img_pln_height / 2.0

        u = l + (r - l) * x / self.nx
        v = b + (t - b) * y / self.ny

        return Ray(self.position, -self.f_len * self.w + u * self.u + v * self.v)


class Scene:
    def __init__(self):
        self.cameras = list()
        self.lights = list()
        self.shapes = list()

        # Create the scene (simpleTwoSphere.xml) without xml parsing
        cam_pos = Vector.from_data([0, 3, 4])
        cam_view = Vector.from_data([0, -1.5, -3])
        self.cameras.append(Camera(cam_pos, cam_view, .4, .5, width, height))

        self.lights.append(Light(Vector.from_data([10, 10, 1]), Vector.from_data([1, 1, 1])))
        self.lights.append(Light(Vector.from_data([10, 8, 1]), Vector.from_data([.8, .8, .8])))
        self.lights.append(Light(Vector.from_data([1, 3, 8]), Vector.from_data([.25, .25, .25])))

        left = Sphere(Vector.from_data([-1.2, 1, -3]), 1.0)
        right = Sphere(Vector.from_data([1.2, 1.1, -4]), 1.1)

        left.shader = BlinnPhong(Vector.from_data([1, 0, 0]), Vector.from_data([1, 1, 1]), 32.0)
        right.shader = Lambertian(Vector.from_data([0, 1, 0]))

        self.shapes.append(left)
        self.shapes.append(right)

    def get_color(self, ray, tmin, tmax, depth):
        hit = Hit()
        if depth >= 0 and self.intersects(ray, tmin, tmax, hit):
            return hit.shader.apply(hit, self, depth - 1)
        return Vector.from_data([.15, .15, .15])

    def intersects(self, ray, tmin, tmax, hit):
        intersected = False
        for shape in self.shapes:
            if shape.intersects(ray, tmin, tmax, hit):
                intersected = True
        return intersected


def main():
    scene = Scene()
    camera = scene.cameras[0]

    pixel_rows = list()
    for i in range(0, width):
        pixel_row = list()
        for j in range(0, height):
            ray = camera.compute_ray(i + 0.5, j + 0.5)

            color = scene.get_color(ray, 1, float('inf'), 3)
            color.clamp(0, 1)

            pixel_row.append(int(color.data[0] * 255))
            pixel_row.append(int(color.data[1] * 255))
            pixel_row.append(int(color.data[2] * 255))
        pixel_rows.append(pixel_row)

    # Write the pixels to disk
    f = open('output.png', 'wb')
    w = png.Writer(width, height)
    w.write(f, pixel_rows)
    f.close()

print 'Started.'
main()
print 'Done.'
