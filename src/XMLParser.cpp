#include "XMLParser.h"
#include <iostream>
#include <regex>

/* We need to parse:
* <?xml version="1.0" encoding="UTF-8" ?>
<scene>
<camera name="main" type="perspective">
<position>2 5 10</position>
<lookatPoint>0 0 0</lookatPoint>
<focalLength>1.0</focalLength>
<imagePlaneWidth>1.0</imagePlaneWidth>
</camera>
*/

namespace sivelab {
    std::vector<Tag> parseXML(std::ifstream& infile) {
        using namespace std;

        vector<Tag> tags;
        regex openTag("<(\\w+)((\\s+\\w+=\"\\w+\")*)\\s*>");
        // <camera name="main" type="perspective">

        regex selfClosingTag("<(\\w+)((\\s+\\w+=\"\\w+\")*)\\s*\\/>");
        // <shader ref="Red" />

        regex closeTag("<\\/(\\w+)\\s*>");
        // </camera>

        // For each line...
        bool matchedTag = false;
        string line;
        while (getline(infile, line)) {
            // handle <tag attributes="">, set matchedTag = false
            if (regex_search(line, openTag)) {

            }

                // handle <tag attributes="" />
            else if (regex_search(line, selfClosingTag)) {

            }

                // handle </tag> where matchedTag == true
            else if (matchedTag && regex_search(line, closeTag)) {

            }


                // handle </tag> where matchedTag == false by returning tags
            else if (!matchedTag && regex_search(line, closeTag)) {

            } else {
                // Do nothing (blank line, comment, or <?xml ..?>)
            }
        }
        return tags;
    }
}
