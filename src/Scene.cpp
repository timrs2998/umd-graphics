#include "Scene.h"

#include <boost/iterator/iterator_facade.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/detail/ptree_implementation.hpp>
#include <boost/property_tree/ptree.hpp>

#include "HitStruct.h"
#include "lights/Area.h"
#include "BvhNode.h"
#include "textures/Texture.h"

namespace sivelab {

    Scene::Scene(std::string filename, int nx, int ny, int N) :
            filename(filename), nx(nx), ny(ny), N(N) {
        bgColor = Vector3D(.15, .15, .15);

        xmlScene.registerCallback("camera", this);
        xmlScene.registerCallback("light", this);
        xmlScene.registerCallback("shader", this);
        xmlScene.registerCallback("sceneParameters", this);
        xmlScene.registerCallback("shape", this);
        xmlScene.registerCallback("instance", this);
        xmlScene.registerCallback("texture", this);
        xmlScene.parseFile(filename);

        root = new BvhNode(shapes, 0, 0, 0);
    }

    Scene::~Scene() {
        for (auto& light : lights) {
            delete light;
            light = NULL;
        }
        for (auto& area : areaLights) {
            delete area;
            area = NULL;
        }
        for (auto& keyValue : shaders) {
            Shader* shader = keyValue.second;
            delete shader;
            shaders[keyValue.first] = NULL;
        }
        for (auto& keyValue : textureMap) {
            Texture* texture = keyValue.second;
            delete texture;
            textureMap[keyValue.first] = NULL;
        }
        delete root;
        root = NULL;
    }

    void Scene::instance(const ptree::value_type& value) {
        if (value.first == "camera") {
            parseCameraData(value);
        } else if (value.first == "shader") {
            parseShaderData(value);
        } else if (value.first == "shape") {
            parseShapeData(value);
        } else if (value.first == "instance") {
            parseShapeData(value);
        } else if (value.first == "light") {
            parseLightData(value);
        } else if (value.first == "sceneParameters" || value.first == "scene") {
            parseSceneData(value);
        } else if (value.first == "texture") {
            parseTextureData(value);
        } else {
            std::cerr << "Unknown object found." << std::endl;
        }
    }

    Vector3D Scene::getColor(const Ray& ray, const double tmin, double& tmax,
                             int depth) const {
        HitStruct hit;
        if (depth >= 0 && closestHit(ray, tmin, tmax, hit)) {
            Vector3D color = hit.shape->getShader()->apply(hit, *this, depth - 1);
            return color;
        }
        return bgColor;
    }

    bool Scene::closestHit(const Ray& ray, const double tmin, double& tmax,
                           HitStruct& h) const {
        bool bounding_box = true;
        if (bounding_box) {
            return root->closestHit(ray, tmin, tmax, h);
        } else {
            bool hit = false;
            for (const auto& shape : shapes) {
                if (shape->closestHit(ray, tmin, tmax, h)) {
                    hit = true;
                }
            }
            return hit;
        }
    }

    bool Scene::anyHit(const Ray& ray, const double tmin,
                       const double tmax) const {
        bool bounding_box = true;
        if (bounding_box) {
            return root->anyHit(ray, tmin, tmax);
        } else {
            for (const auto& shape : shapes) {
                if (shape->anyHit(ray, tmin, tmax)) {
                    return true;
                }
            }
            return false;
        }
    }

    const std::vector<std::shared_ptr<Camera>>& Scene::getCameras(void) const {
        return cameras;
    }

    const std::vector<Light*>& Scene::getLights(void) const {
        return lights;
    }

    const std::vector<Area*>& Scene::getAreaLights(void) const {
        return areaLights;
    }
}
