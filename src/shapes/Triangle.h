#pragma once

#include <Shape.h>
#include <Vector3D.h>
#include "Vertex.h"

namespace sivelab {
    class BBox;
    class Ray;
    struct RasterTriangle;

    class Triangle: public Shape {
    public:
        Triangle(Vector3D a, Vector3D b, Vector3D c);
        Triangle(Vertex a, Vertex b, Vertex c);

        virtual ~Triangle();

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& hit) const override;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const
                override;

        std::vector<RasterTriangle> getTriangles(void) const override;

    private:
        Vertex a, b, c;
    };
}
