#pragma once

#include <Shape.h>
#include "../Matrix4x4.h"

namespace sivelab {
    class BBox;
    class Ray;

    class InstancedObject: public Shape {
    public:
        InstancedObject(Shape* renderable, Matrix4x4 transformation);
        virtual ~InstancedObject();

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& hit) const override;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const
                override;
        Shader* getShader(void) const;
        std::vector<RasterTriangle> getTriangles(void) const override;

    private:
        const Shape* renderable;
        const Matrix4x4 transformation;
        Matrix4x4 minv;
    };

}
