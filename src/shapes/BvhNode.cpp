#include <BvhNode.h>
#include <Vector3D.h>

namespace sivelab {

    BvhNode::BvhNode(const std::vector<Shape*>& shapes, int axis, int p2, int p1) {
        left = NULL;
        right = NULL;

        size_t n = shapes.size();
        if (n <= 0) {
            std::cerr << "Shapes is empty in BvhNode!" << std::endl;
        } else if (n == 1) {
            left = shapes.front();
            right = NULL;
            bbox = shapes.front()->boundingBox();
        } else if (n == 2) {
            left = shapes.front();
            right = shapes.at(1);
            bbox = combine(left->boundingBox(), right->boundingBox());
        } else {
            // Find the bounding box of A
            bbox = shapes.front()->boundingBox();
            for (Shape* shape : shapes) {
                bbox = combine(bbox, shape->boundingBox());
            }

            // Find the midpoint m of the bounding box along the axis
            Vector3D minPoint = bbox.getMinPoint();
            Vector3D maxPoint = bbox.getMaxPoint();
            double m = minPoint[axis] + (maxPoint[axis] - minPoint[axis]) / 2.0;

            // Partition A into lists about m
            std::vector<Shape*> leftShapes, rightShapes;
            for (Shape* shape : shapes) {
                // Find the shape's midpoint
                Vector3D sMinPoint = shape->boundingBox().getMinPoint();
                Vector3D sMaxPoint = shape->boundingBox().getMaxPoint();
                double sm = sMinPoint[axis] + (sMaxPoint[axis] - sMinPoint[axis]) / 2.0;

                // Add to the appropriate list
                if (sm < m) {
                    leftShapes.push_back(shape);
                } else {
                    rightShapes.push_back(shape);
                }
            }

            // Don't let shapes all be in same half for every axis, break partitioning if
            // this happens (CornellBox.xml motivates this fix)
            if (p2 == p1 && p1 == n) {
                if (leftShapes.size() == n) {
                    rightShapes.push_back(leftShapes.back());
                    leftShapes.pop_back();
                } else if (rightShapes.size() == n) {
                    leftShapes.push_back(rightShapes.back());
                    rightShapes.pop_back();
                }
            }

            // Create children
            left = leftShapes.size() > 0 ? new BvhNode(leftShapes, (axis + 1) % 3, p1, n) : NULL;
            right = rightShapes.size() > 0 ? new BvhNode(rightShapes, (axis + 1) % 3, p1, n) : NULL;

            // Create box from children
            if (left == NULL) {
                bbox = right->boundingBox();
            } else if (right == NULL) {
                bbox = left->boundingBox();
            } else {
                bbox = combine(left->boundingBox(), right->boundingBox());
            }
        }
    }

    BvhNode::~BvhNode() {
        delete left;
        delete right;
        left = NULL;
        right = NULL;
    }

    bool BvhNode::closestHit(const Ray& ray, const double tmin, double& tmax,
                             HitStruct& hit) const {
        if (bbox.intersects(ray, tmin, tmax)) {
            bool lhit = false, rhit = false;
            if (left != NULL) {
                lhit = left->closestHit(ray, tmin, tmax, hit);
            }
            if (right != NULL) {
                rhit = right->closestHit(ray, tmin, tmax, hit);
            }
            return lhit || rhit;
        }
        return false;
    }

    bool BvhNode::anyHit(const Ray& ray, const double tmin,
                         const double tmax) const {
        if (bbox.intersects(ray, tmin, tmax)) {
            if (left != NULL && left->anyHit(ray, tmin, tmax)) {
                return true;
            } else if (right != NULL && right->anyHit(ray, tmin, tmax)) {
                return true;
            }
        }
        return false;
    }

    BBox BvhNode::combine(const BBox& a, const BBox& b) const {
        Vector3D minA = a.getMinPoint();
        Vector3D maxA = a.getMaxPoint();
        Vector3D minB = b.getMinPoint();
        Vector3D maxB = b.getMaxPoint();

        double xmin = minA[0] < minB[0] ? minA[0] : minB[0];
        double ymin = minA[1] < minB[1] ? minA[1] : minB[1];
        double zmin = minA[2] < minB[2] ? minA[2] : minB[2];

        double xmax = maxB[0] < maxA[0] ? maxA[0] : maxB[0];
        double ymax = maxB[1] < maxA[1] ? maxA[1] : maxB[1];
        double zmax = maxB[2] < maxA[2] ? maxA[2] : maxB[2];

        return BBox(Vector3D(xmin, ymin, zmin), Vector3D(xmax, ymax, zmax));
    }

}
