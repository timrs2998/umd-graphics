#include <Triangle.h>
#include <RasterTriangle.h>
#include <Vertex.h>
#include "../BBox.h"
#include "../HitStruct.h"
#include "../Ray.h"
#include <boost/range/irange.hpp>

namespace sivelab {

    inline BBox constructBBox(Vector3D a, Vector3D b, Vector3D c) {
        Vector3D minPoint = a;
        Vector3D maxPoint = a;

        for (int axis = 0; axis < 3; axis++) {
            // Find minPoint
            if (a[axis] < minPoint[axis])
                minPoint[axis] = a[axis];
            if (b[axis] < minPoint[axis])
                minPoint[axis] = b[axis];
            if (c[axis] < minPoint[axis])
                minPoint[axis] = c[axis];

            // Find maxPoint
            if (a[axis] > maxPoint[axis])
                maxPoint[axis] = a[axis];
            if (b[axis] > maxPoint[axis])
                maxPoint[axis] = b[axis];
            if (c[axis] > maxPoint[axis])
                maxPoint[axis] = c[axis];
        }
        return BBox(minPoint, maxPoint);
    }

    Triangle::Triangle(Vector3D a, Vector3D b, Vector3D c) {
        this->a = Vertex(a, (b - a).cross(c - a), Vector3D());
        this->b = Vertex(b, (b - a).cross(c - a), Vector3D());
        this->c = Vertex(c, (b - a).cross(c - a), Vector3D());

        bbox = constructBBox(a, b, c);
    }

    Triangle::Triangle(Vertex a, Vertex b, Vertex c) :
            a(a), b(b), c(c) {
        bbox = constructBBox(a.position, b.position, c.position);
    }

    Triangle::~Triangle() {
    }

    bool Triangle::closestHit(const Ray& ray, const double tmin, double& tmax,
                              HitStruct& hit) const {
        Vector3D v0 = this->a.position;
        Vector3D v1 = this->b.position;
        Vector3D v2 = this->c.position;

        double a = v0[0] - v1[0];
        double b = v0[1] - v1[1];
        double c = v0[2] - v1[2];

        double d = v0[0] - v2[0];
        double e = v0[1] - v2[1];
        double f = v0[2] - v2[2];

        double g = ray.getDirection()[0];
        double h = ray.getDirection()[1];
        double i = ray.getDirection()[2];

        double j = v0[0] - ray.getOrigin()[0];
        double k = v0[1] - ray.getOrigin()[1];
        double l = v0[2] - ray.getOrigin()[2];

        double ei_hf = e * i - h * f;
        double gf_di = g * f - d * i;
        double dh_eg = d * h - e * g;
        double ak_jb = a * k - j * b;
        double jc_al = j * c - a * l;
        double bl_kc = b * l - k * c;

        // Compute t
        double M = a * ei_hf + b * gf_di + c * dh_eg;
        double t = -(f * ak_jb + e * jc_al + d * bl_kc) / M;
        if (t < tmin || t > tmax) {
            return false;
        }

        // Compute gamma
        double gamma = (i * ak_jb + h * jc_al + g * bl_kc) / M;
        if (gamma < 0.0 || gamma > 1.0) {
            return false;
        }

        // Compute beta
        double beta = (j * ei_hf + k * gf_di + l * dh_eg) / M;
        if (beta < 0.0 || beta > (1.0 - gamma)) {
            return false;
        }

        if (tmin < t && t < tmax) {
            Vector3D intersection = (1.0 - beta - gamma) * v0 + beta * v1 + gamma * v2;
            Vector3D view = ray.getOrigin() - intersection;
            Vector3D normal = this->a.normal * (1.0 - beta - gamma) + this->b.normal * beta + this->c.normal * gamma;
            hit.set(normal, view, intersection, t, (Shape*) this);
            tmax = t;
            return true;
        }
        return false;
    }

    bool Triangle::anyHit(const Ray& ray, const double tmin,
                          const double tmax) const {
        double a = this->a.position[0] - this->b.position[0];
        double b = this->a.position[1] - this->b.position[1];
        double c = this->a.position[2] - this->b.position[2];

        double d = this->a.position[0] - this->c.position[0];
        double e = this->a.position[1] - this->c.position[1];
        double f = this->a.position[2] - this->c.position[2];

        double g = ray.getDirection()[0];
        double h = ray.getDirection()[1];
        double i = ray.getDirection()[2];

        double j = this->a.position[0] - ray.getOrigin()[0];
        double k = this->a.position[1] - ray.getOrigin()[1];
        double l = this->a.position[2] - ray.getOrigin()[2];

        double ei_hf = e * i - h * f;
        double gf_di = g * f - d * i;
        double dh_eg = d * h - e * g;
        double ak_jb = a * k - j * b;
        double jc_al = j * c - a * l;
        double bl_kc = b * l - k * c;

        // Compute t
        double M = a * ei_hf + b * gf_di + c * dh_eg;
        double t = -(f * ak_jb + e * jc_al + d * bl_kc) / M;
        if (t < tmin || t > tmax) {
            return false;
        }

        // Compute gamma
        double gamma = (i * ak_jb + h * jc_al + g * bl_kc) / M;
        if (gamma < 0.0 || gamma > 1.0) {
            return false;
        }

        // Compute beta
        double beta = (j * ei_hf + k * gf_di + l * dh_eg) / M;
        if (beta < 0.0 || beta > (1.0 - gamma)) {
            return false;
        }

        return tmin < t && t < tmax;
    }

    std::vector<RasterTriangle> Triangle::getTriangles(void) const {
        std::vector<RasterTriangle> rasterTriangles;
        rasterTriangles.push_back(RasterTriangle(a, b, c));
        return rasterTriangles;
    }
}
