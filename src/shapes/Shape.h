#pragma once

#include <RasterTriangle.h>
#include <vector>

#include "../BBox.h"

namespace sivelab {
    struct HitStruct;
    class Ray;
    class Shader;
    class Vector3D;

    class Shape {
    public:
        Shape();
        virtual ~Shape();

        virtual BBox boundingBox(void) const;
        virtual bool closestHit(const Ray& ray, const double tmin, double& tmax,
                                HitStruct& hit) const = 0;
        virtual bool anyHit(const Ray& ray, const double tmin,
                            const double tmax) const = 0;

        virtual std::vector<RasterTriangle> getTriangles(void) const = 0;


        virtual double getS(const Vector3D& point) const;
        virtual double getT(const Vector3D& point) const;

        Shader* getShader(void) const;
        void setShader(Shader* shader);

    protected:
        Shader* shader;
        BBox bbox;
    };
}
