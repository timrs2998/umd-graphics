#include <Box.h>
#include <Vector3D.h>
#include "../BBox.h"
#include <BvhNode.h>
#include "Triangle.h"

namespace sivelab {

    Box::Box(Vector3D minPoint, Vector3D maxPoint){
        // 12 counterclockwise (from outside) triangles
        Vector3D min = minPoint;
        Vector3D max = maxPoint;

        // Front
        triangles.push_back(new Triangle(
                Vector3D(max[0], max[1], max[2]),
                Vector3D(min[0], max[1], max[2]),
                Vector3D(min[0], min[1], max[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(min[0], min[1], max[2]),
                Vector3D(max[0], min[1], max[2]),
                Vector3D(max[0], max[1], max[2])
        ));

        // Right
        triangles.push_back(new Triangle(
                Vector3D(max[0], max[1], max[2]),
                Vector3D(max[0], min[1], max[2]),
                Vector3D(max[0], min[1], min[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(max[0], min[1], min[2]),
                Vector3D(max[0], max[1], min[2]),
                Vector3D(max[0], max[1], max[2])
        ));

        // Top
        triangles.push_back(new Triangle(
                Vector3D(max[0], max[1], max[2]),
                Vector3D(max[0], max[1], min[2]),
                Vector3D(min[0], max[1], min[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(min[0], max[1], min[2]),
                Vector3D(min[0], max[1], max[2]),
                Vector3D(max[0], max[1], max[2])
        ));

        // Left
        triangles.push_back(new Triangle(
                Vector3D(min[0], max[1], max[2]),
                Vector3D(min[0], max[1], min[2]),
                Vector3D(min[0], min[1], min[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(min[0], min[1], min[2]),
                Vector3D(min[0], min[1], max[2]),
                Vector3D(min[0], max[1], max[2])
        ));

        // Bottom
        triangles.push_back(new Triangle(
                Vector3D(min[0], min[1], min[2]),
                Vector3D(max[0], min[1], min[2]),
                Vector3D(max[0], min[1], max[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(max[0], min[1], max[2]),
                Vector3D(min[0], min[1], max[2]),
                Vector3D(min[0], min[1], min[2])
        ));

        // Back
        triangles.push_back(new Triangle(
                Vector3D(max[0], min[1], min[2]),
                Vector3D(min[0], min[1], min[2]),
                Vector3D(min[0], max[1], min[2])
        ));
        triangles.push_back(new Triangle(
                Vector3D(min[0], max[1], min[2]),
                Vector3D(max[0], max[1], min[2]),
                Vector3D(max[0], min[1], min[2])
        ));

        root = new BvhNode(triangles, 0, 0, 0);
        bbox = BBox(minPoint, maxPoint);
    }

    Box::~Box() {
    }

}
