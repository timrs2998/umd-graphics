#pragma once

#include <Shape.h>
#include <vector>

namespace sivelab {
    class BBox;
    class BvhNode;
    class Ray;
    struct RasterTriangle;

    class Mesh: public Shape {
    public:
        Mesh(std::vector<Shape*> triangles);
        virtual ~Mesh();

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& hit) const override;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const
                override;
        std::vector<RasterTriangle> getTriangles(void) const override;

    protected:
        Mesh();
        std::vector<Shape*> triangles;
        BvhNode* root;
    };
}
