#pragma once

#include <Shape.h>
#include <Vector3D.h>

namespace sivelab {
    class BBox;
    class Ray;

    class Sphere: public Shape {
    public:
        Sphere(Vector3D center, double radius);
        virtual ~Sphere();

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& hit) const override;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const
                override;

        double getS(const Vector3D& point) const override;
        double getT(const Vector3D& point) const override;

        std::vector<RasterTriangle> getTriangles(void) const override;
    private:
        const Vector3D center;
        const double radius;
    };

}
