#ifndef RASTERTRIANGLE_H_
#define RASTERTRIANGLE_H_

#include <Vertex.h>
#include <vector>
#include <cassert>

namespace sivelab {

    struct RasterTriangle {
        Vertex vertices[3];

        RasterTriangle(Vertex v0, Vertex v1, Vertex v2) :
                vertices{v0, v1, v2} {
        }

        RasterTriangle(Vector3D v0, Vector3D v1, Vector3D v2) {
            vertices[0] = Vertex(v0, (v1 - v0).cross(v2 - v0), Vector3D());
            vertices[1] = Vertex(v0, (v1 - v0).cross(v2 - v0), Vector3D());
            vertices[2] = Vertex(v0, (v1 - v0).cross(v2 - v0), Vector3D());
        }

        virtual ~RasterTriangle() {
        }

        const Vertex operator[](const int i) const {
            assert(0 <= i && i <= 3);
            return vertices[i];
        }

        Vertex& operator[](const int i) {
            assert(0 <= i && i <= 3);
            return vertices[i];
        }
    };
}

#endif
