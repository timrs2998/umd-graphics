#pragma once

#include <Vector3D.h>

namespace sivelab {

    struct Vertex {
        Vertex() : s(0), t(0) {}

        Vertex(Vector3D position, Vector3D normal, Vector3D color) :
                position(position), normal(normal), color(color), s(0), t(0) {
        }

        Vertex(Vector3D position, Vector3D normal, Vector3D color, double s, double t) :
                position(position), normal(normal), color(color), s(s), t(t) {
        }

        Vector3D position;
        Vector3D normal;
        Vector3D color;
        double s, t;
    };
}
