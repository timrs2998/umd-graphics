#include <Sphere.h>
#include <cmath>
#include <algorithm>
#include "Box.h"
#include "../BBox.h"
#include "../HitStruct.h"
#include "../Ray.h"

namespace sivelab {

    Sphere::Sphere(Vector3D center, double radius) :
            center(center), radius(radius) {
        Vector3D minPoint = center - Vector3D(radius, radius, radius);
        Vector3D maxPoint = center + Vector3D(radius, radius, radius);
        bbox = BBox(minPoint, maxPoint);
    }

    Sphere::~Sphere() {
    }

    bool Sphere::closestHit(const Ray& ray, const double tmin, double& tmax,
                            HitStruct& hit) const {
        Vector3D ro = ray.getOrigin();
        Vector3D rd = ray.getDirection();

        double a = rd.dot(rd);
        double b = (2.0 * rd).dot(ro - center);
        double c = (ro - center).dot(ro - center) - radius * radius;

        double discriminant = b * b - 4.0 * a * c;
        if (discriminant < 0.0)
            return false;

        double t0 = (-b + sqrt(discriminant)) / (2.0 * a);
        double t1 = (-b - sqrt(discriminant)) / (2.0 * a);
        if (t0 > t1) {
            std::swap(t0, t1);
        }

        if (tmin < t0 && t0 < tmax) {			// Case 1: entering sphere
            Vector3D intersection = ro + t0 * rd;
            Vector3D normal = (intersection - center);
            Vector3D view = (ro - intersection);
            hit.set(normal, view, intersection, t0, (Shape*) this);
            tmax = t0;

            return true;
        } else if (tmin < t1 && t1 < tmax) {	// Case 2: exiting sphere
            Vector3D intersection = ro + t1 * rd;
            Vector3D normal = (intersection - center);
            Vector3D view = (ro - intersection);
            hit.set(normal, view, intersection, t1, (Shape*) this);
            tmax = t1;

            return true;
        }
        return false;
    }

    bool Sphere::anyHit(const Ray& ray, const double tmin,
                        const double tmax) const {
        Vector3D ro = ray.getOrigin();
        Vector3D rd = ray.getDirection();

        double a = rd.dot(rd);
        double b = (2.0 * rd).dot(ro - center);
        double c = (ro - center).dot(ro - center) - radius * radius;

        double discriminant = b * b - 4.0 * a * c;
        if (discriminant < 0.0)
            return false;

        double t0 = (-b + sqrt(discriminant)) / (2.0 * a);
        double t1 = (-b - sqrt(discriminant)) / (2.0 * a);
        double t = t0 < t1 ? t0 : t1;

        return tmin < t && t < tmax;
    }

    double Sphere::getS(const Vector3D& point) const {
        double phi = atan2(point[2] - center[2], point[0] - center[0]);
        double u = phi / (2.0 * M_PI);
        return fabs(u);
    }

    double Sphere::getT(const Vector3D& point) const {
        double theta = acos((point[1] - center[1]) / radius);
        double v = (M_PI - theta) / M_PI;
        return fabs(v);
    }

    std::vector<RasterTriangle> Sphere::getTriangles(void) const {
        Vector3D minpoint = center - Vector3D(radius, radius, radius);
        Vector3D maxpoint = center + Vector3D(radius, radius, radius);
        Box box(minpoint, maxpoint);
        return box.getTriangles();
    }

}
