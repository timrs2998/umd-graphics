#pragma once

#include <Shape.h>
#include <vector>

namespace sivelab {
    struct HitStruct;
    class Ray;
    class Shape;
    struct RasterTriangle;

    class BvhNode: public Shape {
    public:
        BvhNode(const std::vector<Shape*>& shapes, int axis, int p2, int p1);
        virtual ~BvhNode();

        bool closestHit(const Ray& ray, const double tmin, double& tmax,
                        HitStruct& hit) const override;
        bool anyHit(const Ray& ray, const double tmin, const double tmax) const
                override;

        std::vector<RasterTriangle> getTriangles(void) const override {
            // TODO: put getTriangles() in RasteriazableShape super class
            throw std::exception();
        }

    private:
        BBox combine(const BBox& a, const BBox& b) const;

        Shape* left;
        Shape* right;
    };

}
