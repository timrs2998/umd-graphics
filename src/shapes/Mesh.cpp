#include <Mesh.h>
#include "../HitStruct.h"
#include <BvhNode.h>
#include "Triangle.h"

namespace sivelab {
    Mesh::Mesh() :
            root(NULL) {
    }

    Mesh::Mesh(std::vector<Shape*> triangles) :
            triangles(triangles) {
        Vector3D minPoint = triangles.at(0)->boundingBox().getMinPoint();
        Vector3D maxPoint = triangles.at(0)->boundingBox().getMaxPoint();

        for (const auto& triangle : triangles) {
            Vector3D a = triangle->boundingBox().getMinPoint();
            Vector3D b = triangle->boundingBox().getMaxPoint();

            for (int axis = 0; axis < 3; axis++) {
                if (a[axis] < minPoint[axis])
                    minPoint[axis] = a[axis];
                if (b[axis] > maxPoint[axis])
                    maxPoint[axis] = b[axis];
            }
        }
        root = new BvhNode(triangles, 0, 0, 0);
        bbox = BBox(minPoint, maxPoint);
    }

    Mesh::~Mesh() {
    }

    bool Mesh::closestHit(const Ray& ray, const double tmin, double& tmax,
                          HitStruct& hit) const {
        bool intersected = false;
        if (root->closestHit(ray, tmin, tmax, hit)) {
            intersected = true;
            hit.shape = this;
        }
        return intersected;
    }

    bool Mesh::anyHit(const Ray& ray, const double tmin, const double tmax) const {
        return root->anyHit(ray, tmin, tmax);
    }

    std::vector<RasterTriangle> Mesh::getTriangles(void) const {
        std::vector<RasterTriangle> rasterTriangles;
        for (const auto& shape : triangles) {
            Triangle* triangle = (Triangle*) shape;
            rasterTriangles.push_back(triangle->getTriangles()[0]);
        }
        return rasterTriangles;
    }

}
