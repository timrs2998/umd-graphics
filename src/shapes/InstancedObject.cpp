#include <InstancedObject.h>
#include "../Ray.h"
#include "../HitStruct.h"

namespace sivelab {

    InstancedObject::InstancedObject(Shape* renderable, Matrix4x4 transformation) :
            renderable(renderable), transformation(transformation) {
        shader = NULL;
        minv = transformation.inverse();

        Vector3D minPoint = renderable->boundingBox().getMinPoint();
        Vector3D maxPoint = renderable->boundingBox().getMaxPoint();

        // Get the 8 vertices of the original (untransformed) bounding box
        std::vector<Vector3D> vertices;
        double a = minPoint[0];
        double b = minPoint[1];
        double c = minPoint[2];
        double d = maxPoint[0];
        double e = maxPoint[1];
        double f = maxPoint[2];
        vertices.push_back(Vector3D(a, b, c));
        vertices.push_back(Vector3D(d, b, c));
        vertices.push_back(Vector3D(d, e, c));
        vertices.push_back(Vector3D(a, e, c));
        vertices.push_back(Vector3D(a, b, f));
        vertices.push_back(Vector3D(d, b, f));
        vertices.push_back(Vector3D(d, e, f));
        vertices.push_back(Vector3D(a, e, f));

        // Transform the 8 vertices and recalculate the min/max points
        for (const auto& vertex : vertices) {
            Vector3D transformed = transformation * vertex;

            for (int axis = 0; axis < 3; axis++) {
                minPoint[axis] =
                        transformed[axis] < minPoint[axis] ?
                        transformed[axis] : minPoint[axis];
                maxPoint[axis] =
                        transformed[axis] < maxPoint[axis] ?
                        maxPoint[axis] : transformed[axis];
            }
        }

        // Set the new bounding box
        bbox = BBox(minPoint, maxPoint);
    }

    InstancedObject::~InstancedObject() {
    }

    bool InstancedObject::closestHit(const Ray& ray, const double tmin,
                                     double& tmax, HitStruct& hit) const {
        // Ray r' = M^-1 r.origin + t M^-t r.direction
        Ray rp(minv * ray.getOrigin(), minv.multVector(ray.getDirection(), 0));

        // rp's direction is not necessarily normalized

        if (renderable->closestHit(rp, tmin, tmax, hit)) {
            Vector3D normal = minv.transpose().multVector(hit.n, 0);
            Vector3D view = -1 * ray.getDirection();
            Vector3D intersection = ray.getOrigin() + hit.t * ray.getDirection();

            // hit.t does not need to be transformed
            hit.set(normal, view, intersection, hit.t, (Shape*) this);

            return true;
        }
        return false;
    }

    bool InstancedObject::anyHit(const Ray& ray, const double tmin,
                                 const double tmax) const {
        // Ray r' = M^-1 r.origin + t M^-t r.direction
        Ray rp(minv * ray.getOrigin(), minv.multVector(ray.getDirection(), 0));

        return renderable->anyHit(rp, tmin, tmax);
    }

    Shader* InstancedObject::getShader(void) const {
        if (this->shader == NULL) {
            return renderable->getShader();
        }
        return this->shader;
    }

    std::vector<RasterTriangle> InstancedObject::getTriangles(void) const {
        // Transform the renderable objects position and normals appropriately
        std::vector<RasterTriangle> triangles = renderable->getTriangles();
        for (auto& triangle : triangles) {
            for (auto& vertex : triangle.vertices) {
                vertex.position = this->minv.inverse() * vertex.position;
                vertex.normal = this->minv.transpose().multVector(vertex.normal, 0);
            }
        }
        return triangles;
    }

}
