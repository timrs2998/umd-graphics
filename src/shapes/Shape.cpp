#include <Shape.h>

namespace sivelab {

    Shape::Shape() {
        shader = NULL;
    }

    Shape::~Shape() {
        // See http://stackoverflow.com/a/13042620/1509183
//	delete shader;
//	shader = NULL;
    }

    BBox Shape::boundingBox(void) const {
        return bbox;
    }

    Shader* Shape::getShader(void) const {
        return shader;
    }

    void Shape::setShader(Shader* shader) {
        this->shader = shader;
    }

    double Shape::getS(const Vector3D& point) const{
        return 0;
    }

    double Shape::getT(const Vector3D& point) const{
        return 0;
    }

}
