#pragma once

#include <Mesh.h>

namespace sivelab {
    class BBox;
    class Ray;
    struct RasterTriangle;

    class Box: public Mesh {
        // TODO: Box should be a special case of Mesh
    public:
        Box(Vector3D minPoint, Vector3D maxPoint);
        virtual ~Box();
    };

}
