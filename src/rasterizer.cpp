#include <png++/image.hpp>

#include <RasterTriangle.h>
#include <Shader.h>
#include <Shape.h>
#include <algorithm>

#include "Basis.h"
#include "Camera.h"
#include "HitStruct.h"
#include "lights/Light.h"
#include "Matrix4x4.h"
#include "Scene.h"

namespace sivelab {

#define ERROR 0

    struct Plane {
        Plane(const Vector3D& a, const Vector3D& b, const Vector3D& c,
              const std::string& name) :
                q(a), name(name) {
            normal = (b - a).cross(c - a);
            d = -1 * (normal.dot(q));
        }

        /** Implicit plane equation, where f(plane) = 0 */
        double f(const Vertex& p) const {
            return normal.dot(p.position) + d;
        }

        bool isOutside(const RasterTriangle& triangle) const {
            double fa = f(triangle[0]);
            double fb = f(triangle[1]);
            double fc = f(triangle[2]);
            return fa > 0 && fb > 0 && fc > 0;
        }

        bool isInside(const RasterTriangle& triangle) const {
            double fa = f(triangle[0]);
            double fb = f(triangle[1]);
            double fc = f(triangle[2]);
            return fa <= ERROR && fb <= ERROR && fc <= ERROR;
        }

        bool spans(const RasterTriangle& triangle) const {
            return !(isOutside(triangle) || isInside(triangle));
        }

        Vector3D normal;
        const Vector3D q;
        double d;
        const std::string name;
    };

    struct ViewFrustum {
        ViewFrustum(double l, double r, double t, double b, double n, double f) :
                l(l), r(r), t(t), b(b), n(n), f(f) {
        }
        std::vector<Plane> getTransformedPlanes(void) const {
            const Matrix4x4 pinv(f, 0, 0, 0, 0, f, 0, 0, 0, 0, 0, f * n, 0, 0, -1,
                                 n + f);
            const Vector3D v0 = pinv * Vector3D(l, b, n);
            const Vector3D v1 = pinv * Vector3D(r, b, n);
            const Vector3D v2 = pinv * Vector3D(l, t, n);
            const Vector3D v3 = pinv * Vector3D(r, t, n);
            const Vector3D v4 = pinv * Vector3D(l, b, f);
            const Vector3D v5 = pinv * Vector3D(r, b, f);
            const Vector3D v6 = pinv * Vector3D(l, t, f);
            const Vector3D v7 = pinv * Vector3D(r, t, f);

            std::vector<Plane> planes;
            planes.push_back(Plane(v0, v1, v2, "near"));
            planes.push_back(Plane(v5, v4, v7, "far"));
            planes.push_back(Plane(v4, v0, v6, "left"));
            planes.push_back(Plane(v1, v5, v3, "right"));
            planes.push_back(Plane(v2, v3, v6, "top"));
            planes.push_back(Plane(v0, v4, v1, "bottom"));
            return planes;
        }
        const double l, r, t, b, n, f;
    };

    inline png::rgb_pixel toPngSpace(Vector3D color) {
        color.clamp(0, 1);
        return png::rgb_pixel((int) (color[0] * 255), (int) (color[1] * 255),
                              (int) (color[2] * 255));
    }

    inline double f01(const double& x0, const double& y0, const double& x1,
                      const double& y1, const double& x, const double& y) {
        return (y0 - y1) * x + (x1 - x0) * y + (x0 * y1) - (x1 * y0);
    }

    inline std::vector<RasterTriangle> shadeVertices(
            const std::vector<RasterTriangle>& triangles, const Scene& scene,
            const std::vector<Light*>& lights, const Shape& shape,
            const Vector3D& position) {
        std::vector<RasterTriangle> shadedTriangles;
        for (const RasterTriangle& triangle : triangles) {
            std::vector<Vertex> vertices;
            vertices.assign(triangle.vertices, triangle.vertices + 3);

            for (Vertex& vertex : vertices) {
                vertex.color = Vector3D();

                // Apply the shader (although still with rays)
                HitStruct hit;
                hit.intersection = vertex.position;
                hit.n = vertex.normal;
                hit.v = (position - hit.intersection);

                hit.n.normalize();
                hit.v.normalize();

                vertex.color = shape.getShader()->apply(hit, scene, 0); 	// with shadows and area light sampling
            }
            shadedTriangles.push_back(
                    RasterTriangle(vertices[0], vertices[1], vertices[2]));
        }
        return shadedTriangles;
    }

    inline std::vector<RasterTriangle> clip(
            const std::vector<RasterTriangle>& input,
            const std::vector<Plane>& planes) {
        using std::swap;
        using std::vector;

        vector<RasterTriangle> output;

        for (const RasterTriangle& triangle : input) {
            bool inside = true;
            Vertex a = triangle[0];
            Vertex b = triangle[1];
            Vertex c = triangle[2];

            for (size_t i = 0; i < planes.size(); i++) {
                const Plane& plane = planes[i];

                double fa = plane.f(a);
                double fb = plane.f(b);
                double fc = plane.f(c);

                if (plane.isOutside(triangle)) {
                    // Triangle is entirely outside plane, skip it
                    inside = false;
                    break;
                } else if (plane.isInside(triangle)) {
                    // Triangle is entirely inside plane, move on to next plane
                    continue;
                }

                // Triangle spans plane, so break the triangle, discard the original
                assert(plane.spans(triangle));
                inside = false;

                if (fa * fc >= 0) {
                    swap(fb, fc);
                    swap(b, c);
                    swap(fa, fb);
                    swap(a, b);
                } else if (fb * fc >= 0) {
                    swap(fa, fc);
                    swap(a, c);
                    swap(fa, fb);
                    swap(a, b);
                }

                // t = (n.a + D) / (n.(a-b))  		For a line segment defined by points a and b
                double ta = -(plane.normal.dot(a.position) + plane.d)
                            / (plane.normal.dot(c.position - a.position));
                double tb = -(plane.normal.dot(b.position) + plane.d)
                            / (plane.normal.dot(c.position - b.position));

                // p = a + t(b - a) 				Intersection b/w line segment and plane
                Vertex A(a.position + ta * (c.position - a.position), a.normal,
                         Vector3D());
                Vertex B(b.position + tb * (c.position - b.position), b.normal,
                         Vector3D());

                RasterTriangle t1(a, b, A);
                RasterTriangle t2(b, B, A);
                RasterTriangle t3(A, B, c);

                // Assert that A and B are inside/on the cube
                //assert(plane.f(A) <= ERROR);// TODO: these fail for other scenes, must match insidePlane(..)
                //assert(plane.f(B) <= ERROR);// If these aren't true, we have an infinite loop.

                vector<RasterTriangle> newInput;
                if (fc >= 0) {	// c is outside
                    newInput.push_back(t1);
                    newInput.push_back(t2);
                } else {		// c is inside
                    newInput.push_back(t3);
                }

                vector<Plane> nextPlanes;
                for (size_t j = i + 1; j < planes.size(); j++) {
                    nextPlanes.push_back(planes[j]);
                }

                newInput = clip(newInput, nextPlanes); // TODO: only use nextPlanes

                for (RasterTriangle newTriangle : newInput) {
                    output.push_back(newTriangle);
                }
                break;
            }
            if (inside) {
                output.push_back(triangle);
            }
        }
        return output;
    }

    /** Gets a list of triangles (with normals, colors, and vertices) from a list of shapes, clipping and shading them. */
    std::vector<RasterTriangle> Scene::shapesToTriangles(
            const ViewFrustum& viewFrustum, const Vector3D& position) const {
        std::vector<RasterTriangle> triangles;

        // Clip and shade the triangles for this shape, then add to our list of triangles
        for (const Shape* shape : shapes) {
            std::vector<RasterTriangle> shapeTriangles = shape->getTriangles();
            // shapeTriangles = clip(shapeTriangles, viewFrustum.getTransformedPlanes());
            shapeTriangles = shadeVertices(shapeTriangles, *this, lights, *shape,
                                           position);

            for (const RasterTriangle& triangle : shapeTriangles) {
                triangles.push_back(triangle);
            }
        }

        return triangles;
    }

    png::image<png::rgb_pixel> Scene::rasterize() const {
        using std::vector;

        const Camera& camera = *getCameras().front();
        const double imagePlaneWidth = camera.getImagePlaneWidth();
        const double imagePlaneHeight = camera.getImagePlaneHeight();
        const double d = camera.getFocalLength();

        // Define the orthographic view volume as [l, r] x [b, t] x [f, n]
        const double r = imagePlaneWidth / 2.0;
        const double l = -r;
        const double t = imagePlaneHeight / 2.0;
        const double b = -t;
        const double n = -d; // z = n, near plane
        const double f = -500; // z = f, far plane (-inf is a bad choice)

        const Vector3D e = camera.getPosition();
        const Vector3D u = camera.getBasis().getU();
        const Vector3D v = -1 * camera.getBasis().getV();
        const Vector3D w = camera.getBasis().getW();

        // Construct Mvp, (Mper = Morth * P), and Mcam for M
        const Matrix4x4 Mvp(nx / 2.0, 0, 0, (nx - 1) / 2.0, 0, ny / 2.0, 0,
                            (ny - 1) / 2.0, 0, 0, 1, 0, 0, 0, 0, 1);
        const Matrix4x4 Morth(2 / (r - l), 0, 0, -(r + l) / (r - l), 0, 2 / (t - b),
                              0, -(t + b) / (t - b), 0, 0, 2 / (n - f), -(n + f) / (n - f), 0, 0,
                              0, 1);
        const Matrix4x4 P(n, 0, 0, 0, 0, n, 0, 0, 0, 0, n + f, -f * n, 0, 0, 1, 0);
        const Matrix4x4 Mcam = Matrix4x4(u[0], u[1], u[2], 0, v[0], v[1], v[2], 0,
                                         w[0], w[1], w[2], 0, 0, 0, 0, 1)
                               * Matrix4x4(1, 0, 0, -e[0], 0, 1, 0, -e[1], 0, 0, 1, -e[2], 0, 0, 0,
                                           1);
        const Matrix4x4 Mper = Morth * P;
        const Matrix4x4 M = Mvp * Mper * Mcam;

        // Paint background color, initialize zbuffer (declare zbuffer on heap for 5000x5000 or larger images)
        png::image<png::rgb_pixel> image(nx, ny);
        double** zbuffer = new double*[ny];
        for (int i = 0; i < nx; i++) {
            zbuffer[i] = new double[nx];
        }
        for (int i = 0; i < nx; i++) { // TODO: boost::irange(..) segfaults here, runs out of stack space.
            for (int j = 0; j < ny; j++) {
                image[ny - 1 - j][i] = toPngSpace(this->bgColor);
                zbuffer[ny - 1 - j][i] = -1;
            }
        }

        const ViewFrustum viewFrustum(l, r, t, b, n, f);
        const vector<RasterTriangle> worldTriangles = shapesToTriangles(viewFrustum,
                                                                        e);

        std::cout << "Rasterization started..." << std::endl;
        vector<RasterTriangle> imageTriangles;

        // Transform world space triangles to 2d image space triangles
        for (const RasterTriangle& triangle : worldTriangles) {
            vector<Vertex> imageVertices;
            for (const Vertex vertex : triangle.vertices) {
                Vector3D vp = M * vertex.position;
                imageVertices.push_back(Vertex(vp, vertex.normal, vertex.color));
            }
            RasterTriangle imageTriangle(imageVertices[0], imageVertices[1],
                                         imageVertices[2]);
            imageTriangles.push_back(imageTriangle);
        }

        // Fragment processing
        for (const RasterTriangle& imageTriangle : imageTriangles) {
            // Construct a 2d bounding box
            int xmin = imageTriangle[0].position[0];
            int xmax = imageTriangle[0].position[0];
            int ymin = imageTriangle[0].position[1];
            int ymax = imageTriangle[0].position[1];

            for (const Vertex& vertex : imageTriangle.vertices) {
                Vector3D point = vertex.position;
                xmin = floor(point[0] < xmin ? point[0] : xmin);
                xmax = ceil(point[0] > xmax ? point[0] : xmax);
                ymin = floor(point[1] < ymin ? point[1] : ymin);
                ymax = ceil(point[1] > ymax ? point[1] : ymax);
            }

            // Let p0 = (x0, y0), ..., p2 = (x2, y2) be the vertices of our triangle
            double x0 = imageTriangle[0].position[0];
            double y0 = imageTriangle[0].position[1];
            double x1 = imageTriangle[1].position[0];
            double y1 = imageTriangle[1].position[1];
            double x2 = imageTriangle[2].position[0];
            double y2 = imageTriangle[2].position[1];

            double f_alpha = f01(x1, y1, x2, y2, x0, y0);
            double f_beta = f01(x2, y2, x0, y0, x1, y1);
            double f_gamma = f01(x0, y0, x1, y1, x2, y2);

            // Shade bounding box pixels that are in triangle
            for (int x = xmin; x < xmax; x++) {
                for (int y = ymin; y < ymax; y++) {
                    if (x < 0 || x >= nx || y < 0 || y >= ny) {
                        continue; // Comment out when clipping is enabled
                    }

                    // Compute alpha, beta, and gamma
                    double alpha = f01(x1, y1, x2, y2, x, y) / f_alpha;
                    double beta = f01(x2, y2, x0, y0, x, y) / f_beta;
                    double gamma = f01(x0, y0, x1, y1, x, y) / f_gamma;

                    // if alpha, beta, and gamma are between 0 and 1
                    if (alpha >= 0 && beta >= 0 && gamma >= 0) {
                        if ((alpha > 0 || f_alpha * f01(x1, y1, x2, y2, -1, -1) > 0)
                            && (beta > 0
                                || f_beta * f01(x2, y2, x0, y0, -1, -1) > 0)
                            && (gamma > 0
                                || f_gamma * f01(x0, y0, x1, y1, -1, -1) > 0)) {
                            double depth = alpha * imageTriangle[0].position[2]
                                           + beta * imageTriangle[1].position[2]
                                           + gamma * imageTriangle[2].position[2];
                            if (zbuffer[ny - 1 - y][x] < depth) {
                                Vector3D c[] = { imageTriangle[0].color,
                                                 imageTriangle[1].color,
                                                 imageTriangle[2].color };
                                Vector3D color = alpha * c[0] + beta * c[1]
                                                 + gamma * c[2];
                                image[ny - 1 - y][x] = toPngSpace(color);
                                zbuffer[ny - 1 - y][x] = depth;
                            }
                        }
                    }
                }
            }
        }

        // Delete zbuffer
        for (int i = 0; i < nx; i++) {
            delete[] zbuffer[i];
        }
        delete[] zbuffer;

        std::cout << "Rasterization finished." << std::endl;
        return image;
    }
}
