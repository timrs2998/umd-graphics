#include <png++/image.hpp>
#include <png++/rgb_pixel.hpp>
#include <RasterTriangle.h>
#include "Scene.h"
#include "Shape.h"
#include "HitStruct.h"

#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <vector>
#include <memory>

namespace sivelab {

    void Scene::hwRasterize() const {
        const Camera& camera = *getCameras().front();

        std::unique_ptr<sf::Window> window = std::unique_ptr < sf::Window
        > (new sf::Window(sf::VideoMode(this->nx, this->ny, 32),
                          "SFML OpenGL"));
        glewInit();
        window->setActive();
        glClearColor(bgColor[0], bgColor[1], bgColor[2], 1.0);

        glEnable(GL_CULL_FACE);

        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
        glEnable(GL_COLOR_MATERIAL);

        glViewport(0, 0, this->nx, this->ny);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        float verticalFieldOfView = 60.0; // in degrees
        float aspectRatio = this->nx / (float) this->ny;
        float nearPlaneDist = 1.0;
        float farPlaneDist = 500.0;
        gluPerspective(verticalFieldOfView, aspectRatio, nearPlaneDist,
                       farPlaneDist);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glFrontFace(GL_CCW);

        Vector3D cameraPosition(camera.getPosition());
        Vector3D cameraLookat(camera.getLookAt());

        /////////////////////////////////////////
        // Create the vbo(s)
        /////////////////////////////////////////

        // TODO: this segment can be multithreaded?
        GLuint stuffId;
        glGenBuffers(1, &stuffId);
        glBindBuffer(GL_ARRAY_BUFFER, stuffId);

        int numTriangles = 0;
        for (const auto* shape : shapes) {
            numTriangles += shape->getTriangles().size();
        }

        const int size = (3 + 3 + 2 + 3) * (3 * numTriangles);
        std::unique_ptr<float[]> host_VertexBuffer(new float[size]);
        int tIdx = 0;

        for (const auto* shape : shapes) {
            auto shapeTriangles = shape->getTriangles();
            for (auto& triangle : shapeTriangles) {
                for (auto& vertex : triangle.vertices) {
                    // Apply the shader
                    HitStruct hit;
                    hit.intersection = vertex.position;
                    hit.n = vertex.normal;
                    hit.v = (camera.getPosition() - hit.intersection);

                    hit.n.normalize();
                    hit.v.normalize();
                    vertex.color = shape->getShader()->apply(hit, *this, 2);

                    // Set the properties in the vertex buffer

                    host_VertexBuffer[tIdx++] = vertex.position[0];
                    host_VertexBuffer[tIdx++] = vertex.position[1];
                    host_VertexBuffer[tIdx++] = vertex.position[2];

                    // Colors
                    host_VertexBuffer[tIdx++] = vertex.color[0];
                    host_VertexBuffer[tIdx++] = vertex.color[1];
                    host_VertexBuffer[tIdx++] = vertex.color[2];

                    // Texture coordinates [0, pngWidth]x[0, pngHeight]
                    host_VertexBuffer[tIdx++] = 0;
                    host_VertexBuffer[tIdx++] = 0;

                    host_VertexBuffer[tIdx++] = vertex.normal[0];
                    host_VertexBuffer[tIdx++] = vertex.normal[1];
                    host_VertexBuffer[tIdx++] = vertex.normal[2];
                }
            }
        }

        int numBytes = size * sizeof(float);
        glBufferData(GL_ARRAY_BUFFER, numBytes, host_VertexBuffer.get(),
                     GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        /////////////////////////////////////////
        // Main application loop
        /////////////////////////////////////////
        while (window->isOpen()) {
            sf::Event event;
            while (window->pollEvent(event)) {
                if (event.type == sf::Event::Closed) {
                    window->close();
                } else if (event.type == sf::Event::KeyPressed) {
                    const double increment = 0.25;

                    // Move camera
                    if (event.key.code == sf::Keyboard::Escape) {
                        window->close();
                    } else if (event.key.code == sf::Keyboard::W
                               || event.key.code == sf::Keyboard::Up) {
                        cameraPosition[2] -= increment;
                        cameraLookat[2] -= increment;
                    } else if (event.key.code == sf::Keyboard::S
                               || event.key.code == sf::Keyboard::Down) {
                        cameraPosition[2] += increment;
                        cameraLookat[2] += increment;
                    } else if (event.key.code == sf::Keyboard::A
                               || event.key.code == sf::Keyboard::Left) {
                        cameraPosition[0] -= increment;
                        cameraLookat[0] -= increment;
                    } else if (event.key.code == sf::Keyboard::D
                               || event.key.code == sf::Keyboard::Right) {
                        cameraPosition[0] += increment;
                        cameraLookat[0] += increment;
                    } else if (event.key.code == sf::Keyboard::Q
                               || event.key.code == sf::Keyboard::PageUp) {
                        cameraPosition[1] -= increment;
                        cameraLookat[1] -= increment;
                    } else if (event.key.code == sf::Keyboard::E
                               || event.key.code == sf::Keyboard::PageDown) {
                        cameraPosition[1] += increment;
                        cameraLookat[1] += increment;
                    }
                } else if (event.type == sf::Event::Resized) {
                    glViewport(0, 0, event.size.width, event.size.height);
                }
            }

            window->setActive();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glLoadIdentity();
            gluLookAt(cameraPosition[0], cameraPosition[1], cameraPosition[2],
                      cameraLookat[0], cameraLookat[1], cameraLookat[2], 0, 1, 0);

            // Render things!
            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_COLOR_ARRAY);
            glEnableClientState(GL_NORMAL_ARRAY);
            {
                glBindBuffer(GL_ARRAY_BUFFER, stuffId);
                glPushMatrix();
                {
                    glMatrixMode( GL_MODELVIEW);

                    glVertexPointer(3, GL_FLOAT, 11 * sizeof(float), 0);
                    glColorPointer(3, GL_FLOAT, 11 * sizeof(float),
                                   (GLvoid*) (3 * sizeof(float)));
                    glTexCoordPointer(2, GL_FLOAT, 11 * sizeof(float),
                                      (GLvoid*) (6 * sizeof(float)));
                    glNormalPointer(GL_FLOAT, 11 * sizeof(float),
                                    (GLvoid*) (8 * sizeof(float)));

                    glDrawArrays(GL_TRIANGLES, 0, 3 * numTriangles);
                }
                glPopMatrix();
                glBindTexture(GL_TEXTURE_RECTANGLE, 0);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
            }
            glDisableClientState(GL_NORMAL_ARRAY);
            glDisableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_VERTEX_ARRAY);

            window->display();
        }
    }
}
