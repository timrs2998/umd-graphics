#include "Ray.h"

namespace sivelab {

    Ray::Ray(Vector3D origin, Vector3D direction) :
            origin(origin), direction(direction) {
    }

    Ray::~Ray() {}

    const Vector3D& Ray::getDirection(void) const {
        return direction;
    }

    const Vector3D& Ray::getOrigin(void) const {
        return origin;
    }

    void Ray::setDirection(const Vector3D& direction) {
        this->direction = direction;
    }

    void Ray::setOrigin(const Vector3D& origin) {
        this->origin = origin;
    }
}
