#pragma once

/**
* Created to reduce dependence on external libraries and boost. Makes
* assumptions such as 'tag always on one line'.
*/
#include <fstream>
#include <map>
#include <vector>

namespace sivelab {
    struct Tag {
        std::map<std::string, std::string> attributes;
        std::vector<Tag> children;
    };

    /**
    * A naive line-based xml parser used to remove boost dependencies and
    * simplify the build process.
    *
    * Example usage:
    * std::ifstream& infile;
    * infile.open("my_scene.xml");
    * std::vector<Tag> tags = parseXML(infile);
    * Tag scene = tags[0];
    * // Use tags.children to construct scene
    * infile.close();
    */
    std::vector<Tag> parseXML(std::ifstream& infile);
}
