#pragma once

#include <Vector3D.h>

namespace sivelab {
class Shader;
class Shape;

struct HitStruct {

	void set(Vector3D normal, Vector3D view, Vector3D intersection, double t, Shape* shape) {
		n = normal;
		v = view;
		n.normalize();
		v.normalize();
		this->intersection = intersection;
		this->t = t;
		this->shape = shape;
	}

	Vector3D n;
	Vector3D v;
	double t;
	Vector3D intersection;
	const Shape* shape;
};
}
