#include "Camera.h"
#include "Ray.h"

namespace sivelab {

    Camera::Camera(Vector3D position, Vector3D viewDir, double focalLength,
                   double imagePlaneWidth, int nx, int ny) :
            e(position), focalLength(focalLength), imagePlaneWidth(imagePlaneWidth), nx(
            nx), ny(ny) {
        lookat = position + viewDir;
        imagePlaneHeight = this->nx / this->ny * imagePlaneWidth;
        basis = Basis(viewDir, Vector3D(0, 1, 0));
    }

    Camera::Camera(Vector3D position, double focalLength, double imagePlaneWidth,
                   int nx, int ny, Vector3D lookatPoint) :
            e(position), focalLength(focalLength), imagePlaneWidth(imagePlaneWidth), nx(
            nx), ny(ny), lookat(lookatPoint) {
        imagePlaneHeight = this->nx / this->ny * imagePlaneWidth;
        basis = Basis(lookatPoint - position, Vector3D(0, 1, 0));
    }

    Camera::~Camera() {
    }

    void Camera::computeRay(Ray& ray, const double x, const double y) const {
        double l = -imagePlaneWidth / 2.0;
        double r = imagePlaneWidth / 2.0;
        double b = -imagePlaneHeight / 2.0;
        double t = imagePlaneHeight / 2.0;

        double u = l + (r - l) * x / nx;
        double v = b + (t - b) * y / ny;

        ray.setDirection(
                -focalLength * basis.getW() + u * basis.getU() + v * basis.getV());
    }

    const Vector3D& Camera::getPosition() const {
        return e;
    }

    const Basis& Camera::getBasis() const {
        return basis;
    }

    const Vector3D& Camera::getLookAt() const {
        return lookat;
    }

    const double Camera::getFocalLength() const {
        return focalLength;
    }

    double Camera::getImagePlaneHeight() const {
        return imagePlaneHeight;
    }

    const double Camera::getImagePlaneWidth() const {
        return imagePlaneWidth;
    }

}
