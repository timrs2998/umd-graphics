#pragma once

#include "../Basis.h"
#include "Light.h"
#include <Vector3D.h>

// http://www.cs.dartmouth.edu/~fabio/teaching/graphics09/lectures/16_DistributionRayTracing.pdf
namespace sivelab {

    class Area: public Light {
    public:
        Area(Vector3D position, Vector3D intensity, Vector3D normal, double width,
             double length, int N);
        virtual ~Area();

        Vector3D getPosition(double x, double y) const;
        const int getN() const;

    private:
        const int N;
        const double width, length;
        const Basis basis;
    };

}
