#include "Area.h"

namespace sivelab {

    Area::Area(Vector3D position, Vector3D intensity, Vector3D normal, double width,
               double length, int N) :
            Light(position, intensity), N(N), width(width), length(length),
            basis(Basis(normal)) {
    }

    Area::~Area() {
    }

    Vector3D Area::getPosition(double x, double y) const {
        double l = -width / 2.0;
        double r = width / 2.0;
        double b = -length / 2.0;
        double t = length / 2.0;

        double u = l + (r - l) * x / N;
        double v = b + (t - b) * y / N;
        return Light::getPosition() + u * basis.getU() + v * basis.getV();
    }

    const int Area::getN() const {
        return N;
    }
}

