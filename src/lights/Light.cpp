#include "Light.h"

namespace sivelab {

	Light::Light(Vector3D position, Vector3D intensity) : position(position), intensity(intensity) {
	}

	Light::~Light() {
	}

	const Vector3D& Light::getIntensity(void) const {
		return intensity;
	}

const Vector3D& Light::getPosition(void) const {
	return position;
}

}
