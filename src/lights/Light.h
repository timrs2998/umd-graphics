#pragma once

#include <Vector3D.h>

namespace sivelab {

    class Light {
    public:
        Light(Vector3D position, Vector3D intensity);

        virtual ~Light();

        const Vector3D &getIntensity(void) const;

        const Vector3D &getPosition(void) const;

    private:
        const Vector3D position;
        const Vector3D intensity;
    };

}
