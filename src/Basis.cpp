#include "Basis.h"

namespace sivelab {
    Basis::Basis() :
            u(Vector3D(1, 0, 0)), v(Vector3D(0, 1, 0)), w(Vector3D(0, 0, 1)) {
    }

    Basis::Basis(Vector3D a) {
        w = -1 * a;
        w.normalize();

        Vector3D t(w[0], w[1], w[2]);
        int smallest = 0;
        for (int i = 0; i < 3; i++) {
            if (t[i] < t[smallest]) {
                smallest = i;
            }
        }
        t[smallest] = 1;
        init(a, t);
    }

    Basis::Basis(Vector3D a, Vector3D b) {
        init(a, b);
    }

    Basis::~Basis() {
    }

    void Basis::init(Vector3D a, Vector3D b) {
        w = -1 * a;
        w.normalize();

        u = w.cross(b);
        if (areColinear(w, b)) {
            double epsilon = 1e-3;
            u = w.cross(Vector3D(b[0] + epsilon, b[1] + epsilon, b[2] + epsilon));
        }
        u.normalize();

        v = w.cross(u);
        v.normalize();

        u = -1 * u;
    }

    const Vector3D& Basis::getU() const {
        return u;
    }

    const Vector3D& Basis::getV() const {
        return v;
    }

    const Vector3D& Basis::getW() const {
        return w;
    }

    bool Basis::areColinear(const Vector3D& a, const Vector3D& b) const {
        Vector3D a_norm = a / a.length();
        Vector3D b_norm = b / b.length();
        double epsilon = 1e-3;
        return fabs(a_norm[0] - b_norm[0]) < epsilon
               && fabs(a_norm[1] - b_norm[1]) < epsilon
               && fabs(a_norm[2] - b_norm[2]) < epsilon;
    }

}
