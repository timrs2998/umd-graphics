#include <iostream>
#include <cmath>
#include <vector>
#include <limits>
#include <ctime>
#include <memory>
#include <random>
#include <thread>

#include "png++/png.hpp"
#include "handleGraphicsArgs.h"

#include "Camera.h"
#include "Ray.h"
#include "Scene.h"

using namespace sivelab;

/** Worker thread that renders pixels, has main raytracer loop */
class Worker {
public:
    Worker(png::image<png::rgb_pixel>* imageData, int start, int n, int N,
           int depth, const Scene& scene, const Camera& camera) :
            imageData(imageData), start(start), n(n), N(N), depth(depth), scene(
            scene), camera(camera) {
    }

    void operator()() {
        const double tmin = 1;
        Ray ray(camera.getPosition(), Vector3D());

        for (int j = 0; j < (int) imageData->get_height(); j++) {
            for (int i = start; i < imageData->get_width(); i += n) {
                Vector3D color;

                // Apply jittered/stratified sampling
                for (int p = 0; p < N; p++) {
                    for (int q = 0; q < N; q++) {
                        camera.computeRay(ray, i + (p + drand48()) / (double) N,
                                          j + (q + drand48()) / (double) N);

                        double tmax = std::numeric_limits<double>::max();
                        color += scene.getColor(ray, tmin, tmax, depth);
                    }
                }
                color /= pow(N, 2);
                color.clamp(0, 1);

                (*imageData)[j][i] = png::rgb_pixel((int) (color[0] * 255),
                                                    (int) (color[1] * 255), (int) (color[2] * 255));
            }
        }
    }
private:
    png::image<png::rgb_pixel>* imageData;
    const size_t start;
    const int n, N, depth;
    const Scene& scene;
    const Camera& camera;
};

/** Main entry point, parses arguments, loads scene, renders image */
int main(int argc, char *argv[]) {
    GraphicsArgs args;
    args.reg("method", "method to render file (ray, raster, or gl)",
             ArgumentParsing::STRING, 'm');
    args.process(argc, argv);

    std::string method;
    args.isSet("method", method);

    int width = args.width;
    int height = args.height;
    int n = args.isSet("numcpus") ? args.numCpus : 4;
    int depth = args.isSet("recursionDepth") ? args.recursionDepth : 2;
    int rpp = args.isSet("rpp") ? args.rpp : 1;

    std::string input;
    if (args.inputFileName != "") {
        input = args.inputFileName;
    } else {
        std::cerr << "Need input file!" << std::endl;
        return 1;
    }

    std::string output("output.png");
    if (args.outputFileName != "") {
        output = args.outputFileName;
    }

    // Enable raytracing if the user specifies it
    Scene* scene = new Scene(input, width, height, sqrt(rpp));

    clock_t startTime = clock();
    if (method == "ray" || method == "raytracing" || method == "") {
        Camera camera = *(scene->getCameras().front().get());
        png::image<png::rgb_pixel>* image = new png::image<png::rgb_pixel>(
                width, height);

        // Start n threads
        std::cout << "Raytracing started..." << std::endl;
        std::vector<std::thread*> threads(n);
        for (int i = 0; i < n; i += 1) {
            Worker worker(image, i, n, sqrt(rpp), depth, *scene, camera);
            threads[i] = new std::thread(worker);
        }

        // Wait for threads to finish, write image
        for (std::thread* thread : threads) {
            thread->join();
        }
        image->write(output);

        delete image;
    } else if (method == "rast" || method == "raster" || method == "rasterize"
               || method == "rasterizing") {
        // Software rasterizing
        png::image<png::rgb_pixel> image = scene->rasterize();
        image.write(output);
    } else if (method == "opengl") {
        scene->hwRasterize();
    } else {
        std::cerr << "Unknown rendering method." << std::endl;
        std::cout << "Execution finished." << std::endl;
        exit(EXIT_SUCCESS);
    }

    delete scene;

    clock_t endTime = clock();
    double time = (endTime - startTime) / CLOCKS_PER_SEC;
    std::cout << "Wrote image in " << time << " seconds." << std::endl;
    std::cout << "Execution finished." << std::endl;
    return 0;
}
