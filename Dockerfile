FROM gcc:7.2.0 AS builder
RUN apt-get -y update
RUN apt-get -y install \
  cmake \
  libpng-dev \
  libz-dev \
  libboost-all-dev \
  libxi-dev \
  libxmu-dev \
  freeglut3-dev \
  libglew-dev \
  mesa-utils \
  libsfml-dev

RUN mkdir /app
WORKDIR /app
COPY . .

WORKDIR build/
RUN cmake ..
RUN make --jobs=2

FROM scratch
LABEL MAINTAINER="Tim Schoenheider <timrs2998@gmail.com>"
COPY --from=builder /app/build/src/renderer .
CMD ["./renderer"]
